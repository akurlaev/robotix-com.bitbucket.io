// news
window.addEventListener('load', inputApp);

function inputApp() {
    let containerTag = document.getElementsByClassName('group')[0];
    let html = '';
    let dateAndData = {};
    let lastDate = 0;
    
    for(i = 0; i < news.length; i++) {
        let oneNews = news[i];
        let newsDate = oneNews.date;
        if(dateAndData[newsDate] != undefined) {
            dateAndData[newsDate + lastDate] = i;
            lastDate += 1;
            continue;
        }
        dateAndData[newsDate] = i;
        lastDate = 1
    }
    
    let dateAndDataNames = Object.keys(dateAndData);
    dateAndDataNames.sort();
    
    for(let i = news.length - 1; i >= 0; i--) {
        if (news.length - i - 1 == window.newsRowAmount * 3) {
            break;
        }
        let newsNumber = dateAndData[dateAndDataNames[i]];
        let oneNews = news[newsNumber];
        let textNews = window.newsTemplate;
        let classFirst = '';
        if((news.length - 1 - i) % 3 == 0) {
            classFirst = ' first'
        }
        let replaceList = [
            {
                replaceFrom: '{{CLASSFIRST}}',
                replaceTo: classFirst,
                amount: 1
            },
            {
                replaceFrom: '{{HREF}}',
                replaceTo: oneNews.href,
                amount: 2
            },
            {
                replaceFrom: '{{IMAGE}}',
                replaceTo: oneNews.img,
                amount: 1
            },
            {
                replaceFrom: '{{DAY}}',
                replaceTo: oneNews.day,
                amount: 1
            },
            {
                replaceFrom: '{{MONTH}}',
                replaceTo: oneNews.month,
                amount: 1
            },
            {
                replaceFrom: '{{HEADER}}',
                replaceTo: oneNews.header,
                amount: 1
            },
            {
                replaceFrom: '{{CONTENT}}',
                replaceTo: oneNews.content,
                amount: 1
            }
        ]
        textNews = replaceAll(textNews, replaceList);
        html += textNews;
    }
    
    containerTag.innerHTML = html;
}
