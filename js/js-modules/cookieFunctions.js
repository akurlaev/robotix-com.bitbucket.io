window.Cookie = CookieFunctions();

function CookieFunctions() {
  return {
    get: function (cookieName) {
      if (cookieName == '') {
        return '';
      }
      let cookies = document.cookie;
      let cookiesAfterSplit = cookies.split(cookieName + '=')[1] || '';
      let cookie = cookiesAfterSplit.split('; ')[0];
      return decodeURI(cookie);
    }
  }
}