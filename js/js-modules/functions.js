//include(`${window.active == 0 ? '' : window.header[window.active].robo != undefined ? '../.' : '.'}./js/js-modules/functions.js`)

function replaceAll(template, replaceList) { //template = string; replaceList = [{replaceFrom: string, replaceTo: string}]
    for (let i = 0; i < replaceList.length; i++) {
        let replaceData = replaceList[i];
        let data = replaceData.replaceTo;
        let replaceFrom = replaceData.replaceFrom;
        for (let j = 0; true; j++) {
            if (template.indexOf(replaceData.replaceFrom) == -1) {
                break;
            }
            template = template.replace(replaceFrom, data);
        }
    }
    return template;
}

function include(url, func) {
    let script = document.createElement('script');
    script.src = url;
    script.setAttribute('onload', func);
    document.getElementsByTagName('body')[0].appendChild(script);
}

function goTo(path, newWin) {
    if (newWin) {
        window.open(path);
    }
    else {
        document.location.href = path;
    }
}

function renderTable(array, containerId, cols) {
    let table = document.createElement('table');
    table.classList.add('tovar-table__row')
    for (let i = 0; i < array.length; i++) {
        let row = document.createElement('tr');
        row.classList.add('tovar-table__row')
        for (let j = 0; j < array[i].length; j++) {
            let cell = document.createElement(array[i].length == 1 ? 'th' : 'td');
            cell.innerText = array[i][j];
            cell.classList.add('tovar-table__cell');
            if (array[i].length == 1) {
                cell.colSpan = cols;
            }
            row.appendChild(cell);
        }
        table.appendChild(row);
    }
    document.getElementById(containerId).appendChild(table);
}

function appendAds(containerId, index) {
    let ads = [
        `<!-- Yandex.RTB R-A-659817-1 -->
        <div id="yandex_rtb_R-A-659817-1"></div>
        <script type="text/javascript">
            (function(w, d, n, s, t) {
                w[n] = w[n] || [];
                w[n].push(function() {
                    Ya.Context.AdvManager.render({
                        blockId: "R-A-659817-1",
                        renderTo: "yandex_rtb_R-A-659817-1",
                        async: true
                    });
                });
                t = d.getElementsByTagName("script")[0];
                s = d.createElement("script");
                s.type = "text/javascript";
                s.src = "//an.yandex.ru/system/context.js";
                s.async = true;
                t.parentNode.insertBefore(s, t);
            })(this, this.document, "yandexContextAsyncCallbacks");
        </script>`, //poster
        `<!-- Yandex.RTB R-A-659817-2 -->
        <div id="yandex_rtb_R-A-659817-2"></div>
        <script type="text/javascript">
            (function(w, d, n, s, t) {
                w[n] = w[n] || [];
                w[n].push(function() {
                    Ya.Context.AdvManager.render({
                        blockId: "R-A-659817-2",
                        renderTo: "yandex_rtb_R-A-659817-2",
                        async: true
                    });
                });
                t = d.getElementsByTagName("script")[0];
                s = d.createElement("script");
                s.type = "text/javascript";
                s.src = "//an.yandex.ru/system/context.js";
                s.async = true;
                t.parentNode.insertBefore(s, t);
            })(this, this.document, "yandexContextAsyncCallbacks");
        </script>`, //setka
        `<!-- Yandex.RTB R-A-659817-3 -->
        <div id="yandex_rtb_R-A-659817-3"></div>
        <script type="text/javascript">
            (function(w, d, n, s, t) {
                w[n] = w[n] || [];
                w[n].push(function() {
                    Ya.Context.AdvManager.render({
                        blockId: "R-A-659817-3",
                        renderTo: "yandex_rtb_R-A-659817-3",
                        async: true
                    });
                });
                t = d.getElementsByTagName("script")[0];
                s = d.createElement("script");
                s.type = "text/javascript";
                s.src = "//an.yandex.ru/system/context.js";
                s.async = true;
                t.parentNode.insertBefore(s, t);
            })(this, this.document, "yandexContextAsyncCallbacks");
        </script>`, //сетка
        `<!-- Yandex.RTB R-A-659817-4 -->
        <div id="yandex_rtb_R-A-659817-4"></div>
        <script type="text/javascript">
            (function(w, d, n, s, t) {
                w[n] = w[n] || [];
                w[n].push(function() {
                    Ya.Context.AdvManager.render({
                        blockId: "R-A-659817-4",
                        renderTo: "yandex_rtb_R-A-659817-4",
                        async: true
                    });
                });
                t = d.getElementsByTagName("script")[0];
                s = d.createElement("script");
                s.type = "text/javascript";
                s.src = "//an.yandex.ru/system/context.js";
                s.async = true;
                t.parentNode.insertBefore(s, t);
            })(this, this.document, "yandexContextAsyncCallbacks");
        </script>`, //сетка
        `<!-- Yandex.RTB R-A-659817-5 -->
        <div id="yandex_rtb_R-A-659817-5"></div>
        <script type="text/javascript">
            (function(w, d, n, s, t) {
                w[n] = w[n] || [];
                w[n].push(function() {
                    Ya.Context.AdvManager.render({
                        blockId: "R-A-659817-5",
                        renderTo: "yandex_rtb_R-A-659817-5",
                        async: true
                    });
                });
                t = d.getElementsByTagName("script")[0];
                s = d.createElement("script");
                s.type = "text/javascript";
                s.src = "//an.yandex.ru/system/context.js";
                s.async = true;
                t.parentNode.insertBefore(s, t);
            })(this, this.document, "yandexContextAsyncCallbacks");
        </script>`, //постер
        `<!-- Yandex.RTB R-A-659817-6 -->
        <div id="yandex_rtb_R-A-659817-6"></div>
        <script type="text/javascript">
            (function(w, d, n, s, t) {
                w[n] = w[n] || [];
                w[n].push(function() {
                    Ya.Context.AdvManager.render({
                        blockId: "R-A-659817-6",
                        renderTo: "yandex_rtb_R-A-659817-6",
                        async: true
                    });
                });
                t = d.getElementsByTagName("script")[0];
                s = d.createElement("script");
                s.type = "text/javascript";
                s.src = "//an.yandex.ru/system/context.js";
                s.async = true;
                t.parentNode.insertBefore(s, t);
            })(this, this.document, "yandexContextAsyncCallbacks");
</script>`, // постер
        `<!-- Yandex.RTB R-A-659817-7 -->
        <div id="yandex_rtb_R-A-659817-7"></div>
        <script type="text/javascript">
            (function(w, d, n, s, t) {
                w[n] = w[n] || [];
                w[n].push(function() {
                    Ya.Context.AdvManager.render({
                        blockId: "R-A-659817-7",
                        renderTo: "yandex_rtb_R-A-659817-7",
                        async: true
                    });
                });
                t = d.getElementsByTagName("script")[0];
                s = d.createElement("script");
                s.type = "text/javascript";
                s.src = "//an.yandex.ru/system/context.js";
                s.async = true;
                t.parentNode.insertBefore(s, t);
            })(this, this.document, "yandexContextAsyncCallbacks");
        </script>`, // poster
        `<!-- Yandex.RTB R-A-659817-8 -->
        <div id="yandex_rtb_R-A-659817-8"></div>
        <script type="text/javascript">
            (function(w, d, n, s, t) {
                w[n] = w[n] || [];
                w[n].push(function() {
                    Ya.Context.AdvManager.render({
                        blockId: "R-A-659817-8",
                        renderTo: "yandex_rtb_R-A-659817-8",
                        async: true
                    });
                });
                t = d.getElementsByTagName("script")[0];
                s = d.createElement("script");
                s.type = "text/javascript";
                s.src = "//an.yandex.ru/system/context.js";
                s.async = true;
                t.parentNode.insertBefore(s, t);
            })(this, this.document, "yandexContextAsyncCallbacks");
        </script>`  //poster
    ];
    document.getElementById(containerId).innerHTML += '<div class="ads-container">' + ads[index] + '</div>';
}

window.Query = {
    getByName: (name) => {
        let searchString = document.location.search;
        if (searchString == '' || searchString == '?') {
            return false;
        }
        let answer = searchString.split(name + '=')[1];
        if (answer == undefined) {
            return false;
        }
        return answer.split('&')[0];
    }
}

window.Message = {
    showInfo: (message) => {
        window.notificationCounter = window.notificationCounter || 1;
        document.getElementsByTagName('body')[0].innerHTML += '<div id="notif' + notificationCounter + '" class="notification"><div><i class="fa fa-close" onclick="Message.closeInfo(\'notif' + notificationCounter + '\')"></i></div>' + message + '</div>';
        let script = document.createElement('script')
        script.text = `function setCloseTimeout` + notificationCounter + `() {Message.closeInfo('` + 'notif' + notificationCounter + `');} setTimeout(setCloseTimeout` + notificationCounter + `, 4000);`;
        document.getElementsByTagName('body')[0].appendChild(script);
        window.notificationCounter++
    },
    closeInfo: (id) => {
        document.getElementById(id).remove();
    }
}

function unique(arr) {
    return [...new Set(arr)]; 
}
