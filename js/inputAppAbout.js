window.addEventListener('load', inputApp);

function inputApp() {
  let tag = document.getElementById('about-pages');
  for (let i = 0; i < window.aboutPages.length; i++) {
    tag.innerHTML += '<div><a href="' + window.aboutPages[i].link + '">&#171;' + window.aboutPages[i].header + '&#187;</a>  &#8212;  ' + window.aboutPages[i].description + '</div>';
  }
}