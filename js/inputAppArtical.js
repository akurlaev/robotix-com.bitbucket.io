window.addEventListener('load', renderArticles);

function renderArticles() {
  window.articlesContainersRendered = window.articlesContainersRendered || 0;
  if (window.articlesOnlyLinks) {
    inputAppArticle();
    return;
  }
  let containerTag = document.getElementsByClassName('articles')[window.articlesContainersRendered];
  let html = '';
  let data = window.articles;
  let ids = [];
  for (let i =  0; i < data.length; i++) {
    let dataItem = data[i];
    let template = window.articleTemplate;
    ids.push(dataItem.id);
    let link = document.location.href.split('?')[0].split('#')[0] + '?artId=' + dataItem.id;
    let replaceList = [
      {
        replaceFrom: '{{ID}}',
        replaceTo: dataItem.id
      },
      {
        replaceFrom: '{{DATE}}',
        replaceTo: dataItem.date
      },
      {
        replaceFrom: '{{HEADER}}',
        replaceTo: dataItem.header
      },
      {
        replaceFrom: '{{CONTENT}}',
        replaceTo: dataItem.content + `<div><button style="margin-top: 20px" class="btn" onclick="let div = document.createElement(\'input\'); div.value=\'` + link + `\'; div.id = \'forcopy\'; document.getElementsByTagName(\'body\')[0].appendChild(div); div.select(); try {document.execCommand(\'copy\'); Message.showInfo(\'Скопировано\');} 
            catch(e) {Message.showInfo(\'Что-то пошло не так\')} finally {
              document.getElementById('forcopy').remove();
            }">Копировать ссылку</button></div>`
      }
    ]
    template = replaceAll(template, replaceList);
    html += template;
  }

  containerTag.innerHTML = html;

  inputAppArticle()

  let id = Query.getByName('artId');
  if (id != false && ids.indexOf(id) != -1) {
    linkClick(id);
  }
}

function inputAppArticle() {
  let containerTag = document.getElementsByClassName('articlesLinks')[window.articlesContainersRendered];
  let html = '';
  let dateAndData = {};
  let lastDate = 0;
  let data = window.articlesLinks;
  let newData = [];

  for (let i = 0; i < data.length; i++) {
    if (window.articlesLinksCheckbox != undefined) {
      if (window.articlesLinksCheckbox[i].display == 'none') {
        continue
      }
    }
    if (window.articlesLinksSearch != undefined) {
      if (window.articlesLinksSearch[i].display == 'none') {
        continue;
      }
    }
    if (window.articlesTypes != undefined) {
      if (window.articlesTypes[i].display == 'none') {
        continue;
      }
    }
    if (data[i].display == 'none') {
      continue;
    }
    newData.push(data[i]);
  }

  for(let i = 0; i < newData.length; i++) {
    let dataItem = newData[i];
    let newsDate = dataItem.date;
    if(dateAndData[newsDate] != undefined) {
      dateAndData[newsDate + lastDate] = i;
      lastDate += 1;
      continue;
    }
    dateAndData[newsDate] = i;
  }

  let dateAndDataNames = Object.keys(dateAndData);
  dateAndDataNames.sort();

  for(let i = newData.length - 1; i >= 0; i--) {
    let classFirst = '';
    if((newData.length - 1 - i) % 3 == 0) {
      if (window.newsRowAmount != undefined && (newData.length - 1 - i) / 3 == window.newsRowAmount) {
        break;
      }
      classFirst = ' first'
    }
    let newsNumber = dateAndData[dateAndDataNames[i]];
    let dataItem = newData[newsNumber];
    let template = window.articleLinkTemplate;
    let classArtOrVid = 'fa-file-text';
    if (dataItem.news) {
      classArtOrVid = ' fa-newspaper-o'
    }
    if (dataItem.video) {
      classArtOrVid = ' fa-video-camera';
    }
    if (dataItem.picture) {
      classArtOrVid = 'fa-picture-o';
    }
    let replaceList = [
      {
        replaceFrom: '{{CLASSFIRST}}',
        replaceTo: classFirst
      },
      {
        replaceFrom: '{{HREF}}',
        replaceTo: dataItem.href
      },
      {
        replaceFrom: '{{IMAGE}}',
        replaceTo: (dataItem.img.indexOf('http') != -1 ? dataItem.img : `${active == 0 ? '' : window.header[window.active].robo != undefined ? '../../' : '../'}images/articles/` + dataItem.img)
      },
      {
        replaceFrom: '{{CLASSARTORVID}}',
        replaceTo: classArtOrVid
      },
      {
        replaceFrom: '{{HEADER}}',
        replaceTo: dataItem.header
      },
      {
        replaceFrom: '{{CONTENT}}',
        replaceTo: dataItem.content
      }
    ]
    template = replaceAll(template, replaceList)
    html += template;
  }

  containerTag.innerHTML = html;
  if (window.articlesContainersSeveralMode) {
    window.articlesContainersRendered++;
    addNewArticlesContainer()
  }
}

function addNewArticlesContainer() {
  let files = ['articles']
  if (articlesContainersRendered == files.length+1) {
    return;
  }
  include(`templates/${files[window.articlesContainersRendered-1]}.js`, 'inputAppArticle()');
}

function linkClick(id) {
  if (window.articlesContainersSeveralMode) {
    window.open('pages/articles.html?artId=' + id);
    return;
  }

  let articlesTags = document.getElementsByClassName('article');
  for (let i = 0; i < articlesTags.length; i++) {
    articlesTags[i].classList.add('none');
  }

  document.getElementById(id).classList.remove('none');
  document.location.href = '#' + id;
}