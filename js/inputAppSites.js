window.addEventListener('load', inputApp);

function inputApp() {
  let containerTag = document.getElementsByClassName('preimushestva-container')[0];
  let data = window.sites;
  let html = '';

  for (let i = 0; i < window.sites.length; i++) {
    let template = window.sitesTemplate;
    let dataItem = data[i];
    template = template.replace('{{IMAGE}}', dataItem.image);
    template = template.replace('{{TITLE}}', dataItem.title);
    template = template.replace('{{DESCRIPTION}}', dataItem.description);
    html += template;
  }

  containerTag.innerHTML = html;
}