window.addEventListener('load', inputApp);
document.getElementById('activation').addEventListener('click', activateCod);

function inputApp() {
  let containerTag = document.getElementsByClassName('preimushestva-container')[0];
  let data = window.premium;
  let html = '';

  for (let i = 0; i < data.length; i++) {
    let template = window.premiumTemplate;
    let dataItem = data[i];
    template = template.replace('{{IMAGE}}', dataItem.image);
    template = template.replace('{{TITLE}}', dataItem.title);
    template = template.replace('{{DESCRIPTION}}', dataItem.description);
    html += template;
  }

  containerTag.innerHTML = html;
  html = '<tbody>';

  for (let i = 0; i < window.tarifTable.length; i++) {
    let rowData = window.tarifTable[i];

    if (i == 0) {
      html += rowData;
      continue;
    }

    let row = '<tr>';

    for (let j = 0; j < rowData.length; j++) {
      let tag = `<td class="cell">`;
      let secondTag = '</td>';
      if (i == 0) {
        tag = '<th class="cell">';
        secondTag = '</th>';
      }
      row += tag + rowData[j] + secondTag;
    }

    row += '</tr>'
    html += row;
  }

  document.getElementById('tarif-table').innerHTML = html + '</tbody>';

  switchTab(0);
}

function switchTab(tabIndex) {
  let tabsTags = document.getElementsByClassName('variant__menu-item');
  for (let i = 0; i < tabsTags.length; i++) {
    let color = 'border-bottom-color: #000;';
    if (i == tabIndex) {
      color = 'border-bottom-color: var(--company-color) !important;';
    }
    tabsTags[i].style = color;
  }
  let date = '';

  if (Number(localStorage.status) - 1 == tabIndex) {
    document.getElementsByClassName('expires-container')[0].style['display'] = 'block';
    date = new Date(Number(window.expires));
    document.getElementById('expires').innerText = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
  }
  else {
    document.getElementsByClassName('expires-container')[0].style['display'] = 'none';
  }
  document.getElementById('img').style['background-image'] = window.premiumVariants[tabIndex].img;
  document.getElementById('cost').innerText = window.premiumVariants[tabIndex].cost;
  document.getElementById('costForYear').innerText = window.premiumVariants[tabIndex].costForYear;
  let html = '';
  for (let i = 0; i < window.premiumVariants[tabIndex].complect.length; i++) {
    html += '<li>' + window.premiumVariants[tabIndex].complect[i] + '</li>';
  }
  document.getElementsByClassName('complect-list')[0].innerHTML = html;
}

async function activateCod() {
  let statusTag = document.getElementById('status');
  statusTag.innerHTML = '<i class="fa fa-refresh spinner"></i>Проверяем код...'
  let code = document.getElementById('input-cod').value;
  // Your web app's Firebase configuration


  let docRef = db.collection("codes").doc("codes");
  let doc = ''
  try {
    doc = await docRef.get();
  } catch (error) {
    console.log("Error getting document:", error);
  }
  if (doc.exists) {
    let data = doc.data();
    if (data[sha512(code)] != undefined) {
      window.status = data[sha512(code)];
    }
    else {
      statusTag.innerHTML = '<i class="fa fa-close negative"></i><span class="negative">Неверный код</span>';
      return;
    }
  } else {
    // doc.data() will be undefined in this case
    console.log("No such document!");
    statusTag.innerHTML = '<i class="fa fa-close negative"></i><span class="negative">Неверный код</span>';
    return;
  }

  docRef = db.collection("codes").doc("max-age");
  try {
    doc = await docRef.get();
  } catch (error) {
    console.log("Error getting document:", error);
  }

  if (doc.exists) {
    let data = doc.data();
    window.expires = data[sha512(code)];
  } else {
    // doc.data() will be undefined in this case
    console.log("No such document!");
  }

  let date = new Date(Number(window.expires));
  localStorage.setItem('code', code);
  localStorage.setItem('expires', window.expires);
  localStorage.status = window.status;
  statusTag.innerHTML = `<i class="fa fa-check positive"></i><span class="positive">Подписка RobotiX Premium ${['Обычный', 'Профи', 'VIP'][window.status-1]} до ${date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear()} активирована</span>`;
}