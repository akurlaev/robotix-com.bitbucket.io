inputAppStyle();
function inputAppStyle() {
    let themeClass = localStorage.mainTheme || '';
    if (localStorage.changeThemeOnHolidays !== undefined && !Boolean(Number(localStorage.changeThemeOnHolidays))) {
        return;
    }
    let date = new Date();
    if (date.getMonth() === 9 && date.getDate() > 0 && date.getDate() < 10) {
        document.getElementsByTagName('html')[0].style = '--site-font-family: Dobro; --header-font-family: Dobro; --menu-font-size: 16px;';
    }
    if ((date.getMonth() === 9 && date.getDate() > 24) || (date.getMonth() === 10 && date.getDate() === 1)) {
        themeClass = 'halloween';
        if (document.location.pathname.indexOf('shop') !== -1) {
            document.getElementsByClassName('bgded')[1].style.backgroundImage = 'url("../../images/backgrounds/hallow.png")'
        }
    }
    if (date.getMonth() === 10 && date.getDate() === 4) {
        themeClass = 'united-Russia';
    }
    if (date.getMonth() === 10 && date.getDate() >=6 && date.getDate() <= 14) {
        themeClass = 'red-october';
    }
    if (date.getMonth() === 11 && date.getDate() >= 23 || date.getMonth() === 0 && date.getDate() <= 13) {
        themeClass = 'christmas'
    }
    openTheme(themeClass);
}

function openTheme(themeName) {
    document.getElementsByTagName('html')[0].className = themeName;
    if (themeName === '') {
        return;
    }
    let styleTag = document.createElement('link');
    styleTag.rel = 'stylesheet';
    styleTag.type = 'text/css';
    styleTag.href = (active === 0 ? './' : (document.location.href.split('/').length - 1 - document.location.href.split('/').indexOf('pages') === 2)  ? '../../' : '../') + 'styles/themes/' + themeName + '.css';
    document.getElementsByTagName('head')[0].appendChild(styleTag);
}
