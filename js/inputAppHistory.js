window.addEventListener('load', inputApp);

function inputApp() {
  let tags = document.getElementsByClassName('citaty-container');
  for (let i = 0; i < Math.ceil(window.header.length / window.citatyInOneContainer); i++) {
    let tag = tags[i];
    let citatyInOneCntr = i + 1 === tags.length ? window.header.length - citatyInOneContainer * i : window.citatyInOneContainer / (i === 0 ? 2 : 1);
    for (let j = 0; j < citatyInOneCntr; j++) {
      let add = window.header[i*window.citatyInOneContainer + j].citata.indexOf('&#8220;') === -1;
      tag.innerHTML += '<span style="font-style: italic">' + (add ? '&#8220;' : '') + window.header[i*window.citatyInOneContainer + j].citata + (add ? '&#8221;' : '') + '</span>' + (window.header[i*window.citatyInOneContainer + j].author == '' ? '' : '&#160;&#160;&#160;&#8212;&#160;&#160;&#160;') + window.header[i*window.citatyInOneContainer + j].author + '<br><br>';
    }
  }
}