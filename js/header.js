window.addEventListener('load', inputApp);

function inputApp() {
  let headerContainerTag = document.getElementsByClassName('bgded')[0];
  let template = window.headerTemplate;
  let data = window.header[window.active];
  let pathToMain = '.';
  let pathToPages = './pages';
  if (window.active > 0) {
    pathToMain += '.';
    pathToPages = '.';
  }
  if (data.robo != undefined) {
    pathToMain += '/..';
    pathToPages += '.'
  }
  let replaceList = [
      {
        replaceFrom: '{{CITATA}}',
        replaceTo: data.citata,
        amount: 1
      },
      {
        replaceFrom: '{{AUTHOR}}',
        replaceTo: data.author,
        amount: 1
      },
      {
        replaceFrom: '{{PASSTOMAIN}}',
        replaceTo: pathToMain,
        amount: 3
      },
      {
        replaceFrom: '{{PASSTOPAGES}}',
        replaceTo: pathToPages,
        amount: 20
      },
      {
        replaceFrom: '{{IMAGE}}',
        replaceTo: data.image,
        amount: 1
      }
  ]
  template = replaceAll(template, replaceList);
  headerContainerTag.innerHTML = template;
  let activeTags = document.getElementsByClassName(window.active);
  if (activeTags[0] != undefined) {
    activeTags[0].classList.add('active');
    activeTags[1].classList.add('active');
  }

  document.getElementsByClassName('header__links-button')[0].addEventListener('click', menuButtonClick);
}

function menuButtonClick() {
  let menuTag = document.getElementById('menu');
  const method = menuTag.classList == 'open' ? 'remove' : 'add';
  menuTag.classList[method]('open')
}