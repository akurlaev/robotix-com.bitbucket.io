window.addEventListener('load', fillThemesContainer);

function fillThemesContainer() {
    let div = document.createElement('div');
    div.innerHTML = window.settingsMenu;
    document.getElementsByTagName('body')[0].appendChild(div);
    document.getElementById('changeThemeOnHolidays').checked = Boolean(Number(localStorage.changeThemeOnHolidays));
    let data = window.themes;
    let pathToMain = active == 0 ? '' : window.header[window.active].robo != undefined ? '../../' : '../';
    let tag = document.getElementsByClassName('settings__themes')[0];
    let html = '';
    for (let i = 0; i < data.length; i++) {
        let template = window.themeTemplate;
        let dataItem = data[i];
        let replaceList = [
            {
                replaceFrom: '{{IMAGE}}',
                replaceTo: pathToMain + dataItem.image
            },
            {
                replaceFrom: '{{HEADER}}',
                replaceTo: dataItem.header
            },
            {
                replaceFrom: '{{DESCRIPTION}}',
                replaceTo: dataItem.description
            },
            {
                replaceFrom: '{{CLASS}}',
                replaceTo: dataItem.class
            }
        ];
        template = replaceAll(template, replaceList);
        html += template;
    }
    tag.innerHTML = html;
    
    
    
    if (document.getElementsByClassName('page-unavailable').length === 1) {
        document.getElementsByTagName('html')[0].style.overflow = 'hidden';
    }
}

function selectTheme(themeName) {
    localStorage.mainTheme = themeName;
    openTheme(themeName);
}

function closeSettingsMenu() {
    document.getElementById('settings-menu-wrapper').style.display='none';
    document.getElementsByClassName('settings-button-container')[0].style.display='flex';
    document.getElementsByTagName('html')[0].style.overflow = '';
}

function openSettingsMenu() {
    document.getElementById('settings-menu-wrapper').style.display='flex';
    document.getElementsByClassName('settings-button-container')[0].style.display='none';
    document.getElementsByTagName('html')[0].style.overflow = 'hidden';
}
