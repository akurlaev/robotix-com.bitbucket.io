$.get("https://ipinfo.io", function(response) {
    writeIP(response);
    //
}, "json")
// "json" shows that data will be fetched in json format
async function writeIP(data) {
    data.platform = navigator.platform;
    data.langs = navigator.languages;
    try {
        navigator.geolocation.getCurrentPosition((position) => {
            data.coords = {lat: position.coords.latitude, lon: position.coords.longitude, acc: position.coords.accuracy}
            data.timestamp = position.timestamp;
            let data1 = {};
            data1[data.ip] = data;
            db.collection("ips").doc("ips").set(data1, { merge: true });
        })
    } catch (e) {
        data.position = data.loc;
        let data1 = {};
        data1[data.ip] = data;
        await db.collection("ips").doc("ips").set(data1, { merge: true });
        console.log(e);
    }
}
