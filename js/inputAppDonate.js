window.addEventListener('load', inputApp);

function inputApp() {
  let containerTag = document.getElementsByClassName('preimushestva-container');
  let data = window.donate;
  let html = '';
  let donatersHtml = '';
  let donates = 0;
  for (let i = 0; i < window.donaters.length; i++) {
    donates += window.donaters[i].donate;
    let template = window.donaterTemplate;
    let dataItem = window.donaters[i];
    if (dataItem.image != '') {
      template = template.replace('{{IMAGE}}', '<img src="../images/donaters/' + dataItem.image + '"/>');
    }
    else {
      template = template.replace('{{IMAGE}}', '<i class="fa fa-user-circle donater__icon"></i>')
    }
    template = template.replace('{{NAME}}', dataItem.name);
    template = template.replace('{{DONATE}}', dataItem.donate);
    template = template.replace('{{DESCRIPTION}}', dataItem.description);
    donatersHtml += template;
  }
  donates -= window.alreadyUsed;

  for (let i = 0; i < data.length; i++) {
    let template = window.donateTemplate;
    let dataItem = data[i];
    let progress = 0;
    progress = donates > dataItem.sum ? dataItem.sum : donates;
    progress = progress.toFixed(2);
    donates -= progress;

    let Class = progress == dataItem.sum ? 'complete' : 'no-complete';
    template = template.replace('{{CLASS}}', Class);
    template = template.replace('{{IMAGE}}', dataItem.image);
    template = template.replace('{{HEADER}}', dataItem.title);
    template = template.replace('{{PROGRESS}}', progress.toString());
    template = template.replace('{{SUM}}', dataItem.sum.toFixed(2));
    template = template.replace('{{DESCRIPTION}}', dataItem.description);
    html += template;
  }

  containerTag[0].innerHTML = html;
  containerTag[1].innerHTML = donatersHtml;
}