document.getElementsByClassName('close')[0].addEventListener('click', closeImage)

function openImage(href, header) {
    document.getElementsByClassName('big-image')[0].src = href;
    document.getElementsByClassName('big-image')[0].alt = header;
    document.getElementsByClassName('big-image')[0].title = header;
    document.getElementsByClassName('big-image-container')[0].style['display'] = 'flex';
}

function closeImage() {
    document.getElementsByClassName('big-image-container')[0].style['display'] = 'none';
}

if (document.getElementsByClassName('close')[1] != undefined) {
    document.getElementsByClassName('close')[1].addEventListener('click', closeVideo);
}

function openVideo(href, header) {
    document.getElementsByClassName('big-video')[0].src = href;
    document.getElementsByClassName('big-video')[0].alt = header;
    document.getElementsByClassName('big-video')[0].title = header;
    document.getElementsByClassName('big-video-container')[0].style['display'] = 'flex';
}

function closeVideo() {
    document.getElementsByClassName('big-video-container')[0].style['display'] = 'none';
    document.getElementsByClassName('big-video')[0].pause();
}
