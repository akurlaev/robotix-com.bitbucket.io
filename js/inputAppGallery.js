// gallery
window.addEventListener('load', inputApp);
document.getElementsByClassName('close')[0].addEventListener('click', closeImage)

function inputApp() {
    let containerTag = document.getElementsByClassName('images-container')[0];
    let html = '';
    let data = window.images;

    let dateAndData = {};
    let lastDate = 0;
    for(let i = 0; i < data.length; i++) {
        let dataItem = data[i];
        let dataDate = dataItem.date;
        if(dateAndData[dataDate] != undefined) {
            dateAndData[dataDate + lastDate] = i;
            lastDate += 1;
            continue;
        }
        dateAndData[dataDate] = i;
    }

    let dateAndDataNames = Object.keys(dateAndData);
    dateAndDataNames.sort();

    for(let i = data.length - 1; i >= 0; i--) {
        if((data.length - 1 - i) % 4 == 0) {
            if (window.newsRowAmount != undefined && (data.length - 1 - i) / 4 == window.newsRowAmount) {
                break;
            }
        }
        let dataNumber = dateAndData[dateAndDataNames[i]];
        let dataItem = data[dataNumber];
        let template = window.gallaryTemplate;
        if (window.active == 0 && dataItem.src.indexOf('https') == -1) {
            dataItem.src = dataItem.src.replace('../', '');
        }
        let replaceList = [
            {
                replaceFrom: '{{HEADER}}',
                replaceTo: dataItem.header,
                amount: 3
            },
            {
                replaceFrom: '{{SRC}}',
                replaceTo: dataItem.src,
                amount: 2
            },
            {
                replaceFrom: '{{DATEFORUSER}}',
                replaceTo: dataItem.dateForUser,
                amount: 1
            }
        ]
        template = replaceAll(template, replaceList);
        html += template;
    }

    containerTag.innerHTML = html;
}

function openImage(href, header) {
    document.getElementsByClassName('big-image')[0].src = href;
    document.getElementsByClassName('big-image')[0].alt = header;
    document.getElementsByClassName('big-image')[0].title = header;
    document.getElementsByClassName('big-image-container')[0].style['display'] = 'flex';
}

function closeImage() {
    document.getElementsByClassName('big-image-container')[0].style['display'] = 'none';
}
