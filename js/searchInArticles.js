window.articlesLinksTypes = [];
window.articlesTypes = [];
window.articlesLinksSearch = [];
window.articlesLinksCheckbox = [];

for (let i = 0; i < window.articlesLinks.length; i++) {
    let type = window.articlesLinks[i].news ? 'news' : window.articlesLinks[i].picture ? 'picture' : window.articlesLinks[i].video ? 'video' : 'article';
    if (window.articlesLinksTypes.indexOf(type) == -1) {
        window.articlesLinksTypes.push(type);
    }
    window.articlesLinksSearch[i] = {};
    window.articlesTypes[i] = {};
    window.articlesLinksCheckbox[i] = {};
    window.articlesLinksSearch[i].header = window.articlesLinks[i].header;
    window.articlesLinksSearch[i].content = window.articlesLinks[i].content;
    window.articlesTypes[i].type = type;
    window.articlesTypes[i].display = window.articlesLinks[i].display;
    window.articlesLinksCheckbox[i].category = window.articlesLinks[i].category;
    window.articlesLinksCheckbox[i].display = window.articlesLinks[i].display;
    for (let j = 0; j < window.articles; j++) {
        if (window.articlesLinks.href == window.articles.id) {
            window.articlesLinksSearch[i].contentMain = window.articles[j].content;
        }
    }
}

window.checkboxTag = document.getElementsByClassName('checkbox-container')[1];
window.typeTag = document.getElementsByClassName('checkbox-container')[0];
let html = '';
let categorys = []

for (let i = 0; i < window.articlesLinksCheckbox.length; i++) {
    for (let j = 0; j < window.articlesLinksCheckbox[i].category.length; j++) {
        let category = window.articlesLinksCheckbox[i].category[j];
        if (categorys.indexOf(category) == -1) {
            categorys.push(category);
            html += '<span><input type="checkbox" checked>' + category + '</span>'
        }
    }
}

checkboxTag.innerHTML = html;

for (let i = 0; i < categorys.length; i++) {
    window.checkboxTag.getElementsByTagName("input")[i].addEventListener("change", changeOnCheckbox);
}

let typeNames = {news: 'новости', picture: 'картинки', video: 'видео', article: 'статьи'}
html = ''

for (let i = 0; i < window.articlesLinksTypes.length; i++) {
    html += '<span><input type="checkbox" checked value="' + window.articlesLinksTypes[i] + '">' + typeNames[window.articlesLinksTypes[i]] + '</span>'
}

typeTag.innerHTML = html;

for (let i = 0; i < window.articlesLinksTypes.length; i++) {
    window.typeTag.getElementsByTagName("input")[i].addEventListener("change", changeOnTypeCheckbox);
}

function changeOnTypeCheckbox() {
    let inputs = window.typeTag.getElementsByTagName("input");
    let checkboxData = [];
    for (let i = 0; i < inputs.length; i++) {
        checkboxData[i] = {};
        checkboxData[i].name = inputs[i].value;
        checkboxData[i].value = inputs[i].checked;
    }
    for (let j = 0; j < window.articlesTypes.length; j++) {
        for (let i = 0; i < checkboxData.length; i++) {
            let type = checkboxData[i].name;
            if (checkboxData[i].value) {
                if (window.articlesTypes[j].type != type) {
                    window.articlesTypes[j].display = 'none';
                } else {
                    window.articlesTypes[j].display = 'block';
                    break;
                }
            } else {
                window.articlesTypes[j].display = 'none';
            }
        }
    }
    inputAppArticle();
}

function changeOnCheckbox() {
    let spans = window.checkboxTag.getElementsByTagName("span");
    let checkboxData = [];
    for (let i = 0; i < spans.length; i++) {
        checkboxData[i] = {};
        checkboxData[i].name = spans[i].innerText;
        checkboxData[i].value = spans[i].getElementsByTagName('input')[0].checked;
    }
    for (let i = 0; i < window.articlesLinksCheckbox.length; i++) {
        for (let j = 0; j < checkboxData.length; j++) {
            let category = checkboxData[j].name;
            let k = 0;
            for (k = 0; k < window.articlesLinksCheckbox[i].category.length; k++) {
                if (window.articlesLinks[i].category[k] == category) {
                    if (!checkboxData[j].value) {
                        window.articlesLinksCheckbox[i].display = 'none';
                        continue;
                    }
                    window.articlesLinksCheckbox[i].display = 'block';
                    break;
                }
            }
            if (k < window.articlesLinksCheckbox[i].category.length) {
                break;
            }
        }
    }
    inputAppArticle();
}

document.getElementById('searchInArticles').addEventListener('change', search);
document.getElementsByClassName('search-icon-container')[0].addEventListener('click', search);
document.getElementsByClassName('close-icon-container')[0].addEventListener('click', () => {document.getElementById('searchInArticles').value = ''; search()});

function search(){
    let text = document.getElementById('searchInArticles').value.toLowerCase();
    for(let i = 0; i < window.articlesLinksSearch.length; i++){
        let header = window.articlesLinksSearch[i].header.toLowerCase();
        let content = window.articlesLinksSearch[i].content.toLowerCase();
        if(header.indexOf(text) != -1 || content.indexOf(text) != -1){
            window.articlesLinksSearch[i].display = 'block';
        }
        else{
            window.articlesLinksSearch[i].display = 'none';
        }
    }
    inputAppArticle();
}