window.addEventListener('load', inputApp);

function inputApp() {
    let containerTag = document.getElementsByClassName('group')[0];
    let html = '';
    let dateAndData = {};
    let lastDate = 0;
    
    for(i = 0; i < news.length; i++) {
        let oneNews = news[i];
        let newsDate = oneNews.date;
        if(dateAndData[newsDate] != undefined) {
            dateAndData[newsDate + lastDate] = i;
            lastDate += 1;
            continue;
        }
        dateAndData[newsDate] = i;
        lastDate = 1
    }
    
    let dateAndDataNames = Object.keys(dateAndData);
    dateAndDataNames.sort();
    
    for(i = news.length - 1; i >= 0; i--) {
        let newsNumber = dateAndData[dateAndDataNames[i]];
        let oneNews = news[newsNumber];
        let textNews = window.newsTemplate;
        let classFirst = '';
        if((news.length - 1 - i) % 4 == 0) {
            classFirst = ' first'
        }
        let replaceList = [
            {
                replaceFrom: '{{CLASSFIRST}}',
                replaceTo: classFirst
            },
            {
                replaceFrom: '{{HREF}}',
                replaceTo: oneNews.href
            },
            {
                replaceFrom: '{{IMAGE}}',
                replaceTo: oneNews.img
            },
            {
                replaceFrom: '{{COST}}',
                replaceTo: oneNews.cost
            },
            {
                replaceFrom: '{{VALUTE}}',
                replaceTo: oneNews.valute
            },
            {
                replaceFrom: '{{HEADER}}',
                replaceTo: oneNews.header
            },
            {
                replaceFrom: '{{CONTENT}}',
                replaceTo: oneNews.content
            }
        ]
        textNews = replaceAll(textNews, replaceList)
        html += textNews;
    }
    
    containerTag.innerHTML = html;
}