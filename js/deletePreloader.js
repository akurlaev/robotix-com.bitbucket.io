deletePreloader()
function deletePreloader() {
    if (document.getElementsByClassName('row1').length != 0 && document.getElementsByClassName('row4').length != 0) {
        document.getElementsByClassName('preloader-container')[0].style.display = 'none';
        if (document.getElementsByClassName('page-unavailable').length === 1) {
            goToShop = () => {
                document.location.href = '../shop.html';
            }
            
            setTimeout(goToShop, 15000);
        }
    }
    else {
        setTimeout(deletePreloader, 100);
    }
}
