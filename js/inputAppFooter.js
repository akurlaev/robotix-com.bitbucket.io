window.addEventListener('load', inputApp);

function inputApp(){
    let template = window.footer;
    let html = '';
    let data = window.companyNews;
    let dateAndIndex = {};
    for (let i = 0; i < data.length; i++) {
        dateAndIndex[data[i].date] = i;
    }
    let dates = Object.keys(dateAndIndex);
    dates.sort();
    dates.reverse();
    for (let i = 0; i < dates.length && i < 2; i++) {
        let companyNewsTemplate = window.footerCompanyNewsTemplate;
        let dataItem = data[dateAndIndex[dates[i]]];
        companyNewsTemplate = companyNewsTemplate.replace('{{HEADER}}', dataItem.header);
        companyNewsTemplate = companyNewsTemplate.replace('{{DATE}}', dataItem.date);
        companyNewsTemplate = companyNewsTemplate.replace('{{DATEASTEXT}}', dataItem.dateAsText);
        companyNewsTemplate = companyNewsTemplate.replace('{{TEXT}}', dataItem.content);
        html += companyNewsTemplate;
    }
    template = template.replace('{{COMPANYNEWS}}', html);
    let footerContainerTag = document.getElementById('footerContainer');
    footerContainerTag.innerHTML = template;
}