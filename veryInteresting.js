function renderTable(array, containerId) {
    let table = document.createElement('table');
    table.classList.add('tovar-table__row')
    for (let i = 0; i < array.length; i++) {
        let row = document.createElement('tr');
        row.classList.add('tovar-table__row')
        for (let j = 0; j < array[i].length; j++) {
            let cell = document.createElement(array[i].length == 1 ? 'th' : 'td');
            cell.innerText = array[i][j];
            cell.classList.add('tovar-table__cell');
            if (array[i].length == 1) {
                cell.colSpan = 2;
            }
            row.appendChild(cell);
        }
        table.appendChild(row);
    }
    document.getElementById(containerId).appendChild(table);
}

/*ip and more*/



function createCodes(year, month, lastDay) {
    let codeor = createId();
    let codepro = createId();
    let codevip = createId();
    let expires = new Date(year, month, lastDay, 23, 59, 59);
    expires = expires.getTime();
    return {
        ordinary: codeor,
        pro: codepro,
        vip: codevip,
        expires: expires
    };
}

function createId() {
    let id = '';
    for (i = 0; i < 64; i++) {
        let symbol = Math.floor(Math.random() * 36);
        id += symbol.toString(36);
    }
    return id;
}