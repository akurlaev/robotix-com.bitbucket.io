window.images = [
    {
        src: 'https://million-wallpapers.ru/wallpapers/0/45/444007941129472/zelenyj-gory-pejzazh.jpg',
        header: 'Озеро в горах Алтая',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: '../images/school/plitveckie-lakes.jpg',
        header: 'Плитвецкие озёра',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: '../images/gallery/forijon.jpg',
        header: 'Национальный парк Форийон',
        dateForUser: '28.10.2020',
        date: '2020-10-28'
    },
    {
        src: 'https://classpic.ru/wp-content/uploads/2020/04/44001/cvetushhie-ljupiny-na-novozelandskom-pejzazhe.jpg',
        header: 'Люпины на озере Тепако',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: 'https://avatars.mds.yandex.net/get-zen_doc/1878023/pub_5ea8e03d6135402f7533a623_5ea8e1463c46570d62e7c656/scale_1200',
        header: 'Осень в горах',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: 'https://i.pinimg.com/originals/8e/6b/5f/8e6b5fdbb6c07ecf0a80c7836cf0e87c.jpg',
        header: 'Осенний лес',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: 'https://w-dog.ru/wallpapers/9/3/487910103335440/italiya-toskana-tuman-nebo-polya-dacha-usadba-toskanskij-dream.jpg',
        header: 'Холмы Тосканы',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: '../images/gallery/alps.jpg',
        header: 'Альпийские луга',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: 'https://c.wallhere.com/photos/e7/1d/stream_long_exposure-102792.jpg!d',
        header: 'Осенний водопад',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: 'https://content-9.foto.my.mail.ru/community/supercards/_groupsphoto/h-9770.jpg',
        header: 'Лесные горы в тумане',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: 'https://funart.pro/uploads/posts/2020-04/1585931319_7-p-foni-s-krasivimi-peizazhami-40.jpg',
        header: 'Домик в Шотландии',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: 'https://firebasestorage.googleapis.com/v0/b/robotix-com.appspot.com/o/images%2Fgallery%2FlakeInMountains.jpg?alt=media&token=5c77f1ef-b1ea-4959-bde8-133203f1ac41',
        header: 'Озеро в горах',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: 'https://look.com.ua/pic/201512/1400x1050/look.com.ua-139837.jpg',
        header: 'Озеро в лесу',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: 'https://etoprozhizn.ru/wp-content/uploads/2018/02/oduvanchiki_1_09154917.jpg',
        header: 'Поле одуванчиков в горах',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: 'https://s.zefirka.net/images/2019-09-17/priroda-i-puteshestviya-na-snimkax-kuma-chevika/priroda-i-puteshestviya-na-snimkax-kuma-chevika-2.jpg',
        header: 'Церковь на озере в горах',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: '../images/gallery/waterfall.jpg',
        header: 'Водопад на Плитвецких озёрах ',
        dateForUser: '05.09.2020',
        date: '2020-09-05'
    },
    {
        src: '../images/articles/eva%60sRobot.jpg',
        header: 'Робот Эвелины',
        dateForUser: '17.09.2020',
        date: '2020-09-17'
    },
    {
        src: '../images/gallery/forijon1.jpg',
        header: 'Национальный парк Форийон',
        dateForUser: '28.10.2020',
        date: '2020-10-28'
    },
    {
        src: '../images/gallery/den-narodnogo-edinstva.gif',
        header: 'День народного единства',
        dateForUser: '04.11.2020',
        date: '2020-11-04'
    },
    {
        src: '../images/gallery/red-october-avrora.jpg',
        header: 'День Великой Октябрьской Социалистической Революции',
        dateForUser: '04.11.2020',
        date: '2020-11-07'
    },
    {
        src: '../images/articles/eco1.png',
        header: 'Экология от Миши',
        dateForUser: '23.12.2020',
        date: '2020-12-23'
    },
    {
        src: '../images/special/christmas.jpg',
        header: 'Новый год от Миши',
        dateForUser: '23.12.2020',
        date: '2020-12-23'
    }
];
