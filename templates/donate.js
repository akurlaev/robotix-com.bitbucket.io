window.alreadyUsed = 0;

window.donate = [
  {
    image: 'hamster.png',
    title: 'Корм для хомячка',
    description: 'Пожалейте нашего хомячка!',
    sum: 127
  },
  {
    image: 'programist.png',
    title: 'Прибавка к зарплате <br> js-программиста',
    description: 'На наших сайтах почти весь код создаётся с помощью JavaScript',
    sum: 1500
  },
  {
    image: 'programist.png',
    title: 'Подарок на день <br> рождения нашему <br> текстовику',
    description: 'Давайте скинемся нашему текстовику на подарок, если бы не он, на нашем сайте только продавались бы услуги!',
    sum: 1500
  },
]