window.settingsMenu = `
<div class="settings-button-container">
  <div class="settings-button" onclick="openSettingsMenu()">
    <i class="fa fa-chevron-left settings-button__icon"></i>
  </div>
</div>
<div id="settings-menu-wrapper">

  <div class="settings-button-container__close" onclick="closeSettingsMenu()"><div class="settings-button-container" style="position: absolute;">
    <div class="settings-button">
      <i class="fa fa-chevron-right settings-button__icon"></i>
    </div>
  </div></div>
  <div class="settings-body">
    <div class="settings-menu">
      <div class="settings-menu__item">Тема</div>
      <div class="settings-menu__close-container">
        <i class="fa fa-close settings-menu__close-button" onclick="closeSettingsMenu()"></i>
      </div>
    </div>
      <div class="themes__checkbox">
          <input type="checkbox" checked="checked" onchange="localStorage.changeThemeOnHolidays = this.checked ? 1 : 0; inputAppStyle();" id="changeThemeOnHolidays">
          <label for="changeThemeOnHolidays">менять тему в течении праздников</label>
      </div>
    <div class="settings__themes"></div>
  </div>
</div>`;

window.themeTemplate = `
    <div class="theme">
      <div style="background-image: url('{{IMAGE}}');" class="theme__img"></div>
      <div class="theme__text">
        <h3 class="theme__text_header">{{HEADER}}</h3>
        <div class="theme__text_description">{{DESCRIPTION}}
          <button class="btn theme__text_button" onclick="selectTheme('{{CLASS}}')">Выбрать</button>
        </div>
      </div>
    </div>
`;

window.themes = [
    {
        image: 'images/special/standard.png',
        header: 'Стандартная тема',
        description: 'Белый фон, обычный тёмно-серый шрифт, стандартная цветовая гамма',
        class: ''
    },
    {
        image: 'images/special/dark-theme.png',
        header: 'Тёмная тема',
        description: 'Тёмный фон и светло-серый шрифт... Идеально для глаз программиста в 2 часа ночи!',
        class: 'dark-theme'
    },
    {
        image: 'images/special/christmas.jpg',
        header: 'Новогодняя тема',
        description: 'Новогодний фон, рождественский шрифт для заголовков и завитый шрифт для основного текста!',
        class: 'christmas'
    },
    {
        image: 'images/special/halloween-backgroung.jpg',
        header: 'Жуткая тема!',
        description: 'Мрачный фон, огненный шрифт для заголовков и паутинный шрифт для основного текста! В этой теме содержится дух Хеллоуина! ',
        class: 'halloween'
    },
    {
        image: 'images/special/united-Russia-backgroung-cropped.jpg',
        header: 'День народного единства',
        description: 'Особый фон и оформление меню в стиле флага России',
        class: 'united-Russia'
    },
    {
        image: 'images/special/red-october-background.jpg',
        header: 'День Октябрьской революции!',
        description: 'Фон в революционном стиле, советский шрифт и красный вместо оранжевого цвета!',
        class: 'red-october'
    }
]