window.news = [
    {
        href: 'http://www.robogeek.ru/roboty-spasateli/avtonomnyi-robot-dezinfitsiruet-sklady-s-pomoschyu-ultrafioleta',
        img: 'robo/Dezinfection.jpg',
        day: '1',
        month: 'Июля',
        header: 'Дезинфекция',
        content: 'Автономный робот дезинфицирует склады с помощью ультрафиолета[…]',
        date: '2020-07-01'
    },
    {
      href: 'http://www.robogeek.ru/novosti-kompanii/rosteh-razrabotal-lazernyi-dalnomer-dlya-kvadrokopterov',
      img: 'robo/Dalnomer.jpeg',
      day: '1',
      month: 'Июля',
      header: 'Улучшенное зрение',
      content: 'Ростех разработал лазерный дальномер для квадрокоптеров[…]',
      date: '2020-07-01'
   },
   {
     href: 'http://www.robogeek.ru/iskusstvennyi-intellekt/ii-sistema-videoanalitiki-dlya-obespecheniya-bezopasnosti-na-plyazhah',
     img: 'robo/Spasatel.jpg',
     day: '1',
     month: 'Июля',
     header: 'Спасатель',
     content: 'ИИ поможет спасателям предотвратить несчастные случаи на пляжах[…]',
     date: '2020-07-01'
   },
    {
        href: 'http://www.robogeek.ru/letayuschie-roboty/bespilotniki-zipline-budut-dostavlyat-siz-medikam-ssha',
        img: 'robo/Zipline.png',
        day: '1',
        month: 'Июня',
        header: 'Беспилотники Zipline',
        content: 'Беспилотники Zipline будут доставлять СИЗ медикам США[…]',
        date: '2020-06-01'
    },
    {
        href: 'http://www.robogeek.ru/servisnye-roboty/v-yuzhnokoreiskom-kafe-robotizirovannye-barista-gotovyat-i-podayut-napitki',
        img: 'robo/Barista.png',
        day: '1',
        month: 'Июня',
        header: 'Роботизированные бариста',
        content: 'В южнокорейском кафе роботизированные бариста готовят и подают напитки[…]',
        date: '2020-06-01'
    },
    {
        href: 'http://www.robogeek.ru/avtonomnyi-transport/bespilotnyi-avtomobil-iz-sankt-peterburga-gotovitsya-vyehat-na-dorogi-goroda',
        img: 'robo/StarLine.png',
        day: '1',
        month: 'Июня',
        header: 'Беспилотный автомобиль',
        content: 'Беспилотный автомобиль из Санкт-Петербурга готовится выехать на дороги города[…]',
        date: '2020-06-01'
    },
    {
        href: 'https://robroy.ru/robotizirovannaya-morskaya-cherepaxa-u-cat-budet.html',
        img: 'robo/turtle.png',
        day: '1',
        month: 'Май',
        header: 'Роботизированная черепаха U-CAT',
        content: 'Роботизированная морская черепаха U-CAT будет исследовать водные просторы аквариума[…]',
        date: '2020-05-01'
    },
    {
        href: 'https://robroy.ru/krupnejshij-v-mire-czentr-3d-pechati-gotovitsya.html',
        img: 'robo/3DCovid19.png',
        day: '1',
        month: 'Май',
        header: 'Необходимая помощь',
        content: 'Крупнейший в мире центр 3D-печати готовится помочь защитить врачей от коронавируса[…]',
        date: '2020-05-01'
    },
    {
        href: 'https://robroy.ru/rabotat-s-lyudmi-i-zanimatsya-proverkoj-svarnyix-shvov.html',
        img: 'robo/show.png',
        day: '1',
        month: 'Май',
        header: 'Прочный шов',
        content: 'Работать с людьми и заниматься проверкой сварных швов будет специальная роботизированная система[…]',
        date: '2020-05-01'
    },
    {
        href: 'https://hi--news-ru.turbopages.org/s/hi-news.ru/robots/kogda-roboty-kurery-zamenyat-zhivyx-lyudej.html?utm_source=turbo_turbo',
        img: 'robo/Dostavshik.png',
        day: '15',
        month: 'Августа',
        header: 'Автоматическая доставка',
        content: 'Когда роботы-курьеры заменят живых людей?[…]',
        date: '2020-08-15'
    },
    {
      href: 'http://www.robogeek.ru/letayuschie-roboty/gazprom-neft-ispytala-sverhdalnii-bespilotnik-dlya-razvedochnoi-geofiziki',
      img: 'robo/Gazdrone.png',
      day: '15',
      month: 'Августа',
      header: 'Беспилотная геофизика',
      content: '«Газпром нефть» испытала сверхдальний беспилотник для разведочной геофизики[…]',
      date: '2020-08-15'
   },
   {
     href: 'https://hi--news-ru.turbopages.org/s/hi-news.ru/robots/smotrite-kak-robot-gusenica-pereprygivaet-cherez-prepyatstviya.html?utm_source=turbo_turbo',
     img: 'robo/Lestnica.png',
     day: '15',
     month: 'Августа',
     header: 'Механическая гусеница',
     content: 'Смотрите, как робот-гусеница перепрыгивает через препятствия[…]',
     date: '2020-08-15'
   },
   {
     href: 'https://ria.ru/20200727/1574970854.html',
     img: 'robo/moon.jpg',
     day: '18',
     month: 'Августа',
     header: 'Роботы на Луне',
     content: 'Российскую лунную базу построят роботы, заявил разработчик "Федора"[…]',
     date: '2020-08-18'
   },
  {
    href: 'https://robo-hunter.com/news/robot-nauchilsya-narezat-ovoshi-na-lomtiki-opredelennoi-tolshini16745',
    img: 'robo/cooker.png',
    day: '18',
    month: 'Августа',
    header: 'Робот-повар',
    content: 'Робот научился нарезать овощи на ломтики определенной толщины[…]',
    date: "2020-08-18"
  },
  {
    href: 'https://robo-hunter.com/news/robot-rocycle-sortiruet-plastik-i-bumagu-dlya-pererabotki16862',
    img: 'robo/sort.jpeg',
    day: '18',
    month: 'Августа',
    header: 'Сортировщик мусора',
    content: 'Робот RoCycle сортирует пластик и бумагу для переработки[…]',
    date: "2020-08-18"
  },
  {
    href: 'http://www.robogeek.ru/bytovye-roboty/robot-pumpkii-budet-razvlekat-domashnih-pitomtsev-i-ochischat-za-nimi-tualet',
    img: 'robo/pet.jpg',
    day: '17',
    month: 'Августа',
    header: 'Они и с кошечкой поиграют',
    content: 'Робот Pumpkii будет развлекать домашних питомцев и очищать за ними туалет[…]',
    date: "2020-08-17"
  },
  {
    href: 'https://robo-hunter.com/news/roboti-psi-ot-boston-dynamics-buksiruyt-gruzovik16907',
    img: 'robo/dog.png',
    day: '17',
    month: 'Августа',
    header: 'Собаки буксировщики',
    content: 'Роботы-псы от Boston Dynamics буксируют грузовик[…]',
    date: "2020-08-17"
  },
  {
    href: 'http://www.robogeek.ru/iskusstvennyi-intellekt/atomwise-privlekaet-123-mln-na-poisk-novyh-lekarstv-s-pomoschyu-ii',
    img: 'robo/science.png',
    day: '17',
    month: 'Августа',
    header: 'Роботы-учёные',
    content: 'Atomwise привлекает $123 млн на поиск новых лекарств с помощью ИИ[…]',
    date: "2020-08-17"
  },
  {
    href: 'http://www.robogeek.ru/roboty-v-selskom-hozyaistve/dron-aeroseeder-uskorit-posev-pokrovnyh-kultur',
    img: 'robo/posev.jpg',
    day: '16',
    month: 'Августа',
    header: 'Посадка с высоты',
    content: 'Дрон AeroSeeder ускорит посев покровных культур[…]',
    date: "2020-08-16"
  },
  {
    href: 'https://robotics.ua/news/events/8129-toyota_prodemonstriruet_robotov_pomoshhnikov_na_olimpiade_v_tokio_2020',
    img: 'robo/olimp.jpg',
    day: '16',
    month: 'Августа',
    header: 'Роботы на олимпиаде',
    content: 'Олимпийские игры 2020 в Токио будут обслуживать Роботы[…]',
    date: "2020-08-16"
  },
  {
    href: 'https://hi-news.ru/robots/moralnyj-kodeks-robota-vozmozhno-li-takoe.html/amp',
    img: 'robo/moral.png',
    day: '16',
    month: 'Августа',
    header: 'Им стыдно?',
    content: 'Моральный кодекс робота: возможно ли такое?[…]',
    date: "2020-08-16"
  }
  //http://www.robogeek.ru/internet-veschei/avtomatika-predstavila-reshenie-dlya-goroda-na-baze-interneta-veschei
  //https://vpk.name/news/432957_voennye_nauchilis_peredavat_sekretnuyu_informaciyu_s_pomoshyu_bespilotnikov.html
];
