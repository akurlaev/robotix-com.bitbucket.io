window.donateTemplate = `<div class="preimushestvo-wrapper">
                           <div class="znachok-background">
                             <img src="../images/donate/{{IMAGE}}"/>
                             <div class="{{CLASS}}">
                               <i class="fa fa-check"></i>
                             </div>
                           </div>
                           <h3>{{HEADER}}:<br>{{PROGRESS}}/{{SUM}} рублей</h3>
                           {{DESCRIPTION}}
                         </div>`
window.donaterTemplate = `<div class="donater">
                           <div class="donater__icon-background">
                             {{IMAGE}}
                           </div>
                           <h3>{{NAME}}:<br>{{DONATE}} рублей</h3>
                           <span class="donater__citata">&#8220;{{DESCRIPTION}}&#8221;</span>
                         </div>`