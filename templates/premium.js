window.premium = [
  {
    title: 'Без рекламы',
    image: 'ads.png',
    description: 'На нашем сайте исчезнет реклама (только на этом устройстве)'
  },
  {
    title: 'Скидки на услуги',
    image: 'sale.png',
    description: 'На Вашу почту будут приходить письма с промо-кодами'
  },
  {
    title: 'Уменьшение процентов',
    image: 'procent.png',
    description: 'В зависимости от тарифа, проценты по вашим задолжностям будут уменьшаться до 3% (<a onclick="switchTab(1)" href="#tarifs">Профи</a>) или до 1% (<a onclick="switchTab(2)" href="#tarifs">VIP</a>)'
  },
  {
    title: 'Минимальные <br>сроки работы',
    image: 'watch.png',
    description: 'Мы бросим все силы на реализацию вашего заказа'
  },
  {
    title: 'Обслуживание <br>в первую очередь',
    image: 'vip.png',
    description: 'Мы будем читать и отвечать на ваши письма, выполнять ваши заказы в первую очередь'
  }
]

window.premiumVariants = [
  {
    img: 'url(../images/premium/ads.png)',
    cost: '49.99 рублей',
    costForYear: '499.99 рублей',
    complect: ['Отключение рекламы', 'Очередь обслуживания 3']
  },
  {
    img: 'url(../images/premium/profi.png)',
    cost: '119.99 рублей',
    costForYear: '1199.99 рублей',
    complect: ['Отключение рекламы', 'Скидки на услуги', 'Уменьшение процентов по задолжностям (до 4%)', 'Очередь обслуживания 2']
  },
  {
    img: 'url(../images/premium/vip1.png)',
    cost: '249.99 рублей',
    costForYear: '2499.99 рублей',
    complect: ['Отключение рекламы', 'Скидки на услуги', 'Уменьшение процентов по задолжностям (до 1%)', 'Минимальные сроки работ', 'Очередь обслуживания 1']
  },
]

window.tarifTable = [
  '<tr><th rowspan="2" class="cell first-cell">Составляющие</th><th class="cell" colspan="3">Тариф</th><tr><th class="cell">Обычный</th><th class="cell">Профи</th><th class="cell">VIP</th></tr>',
  [
    'Отключение рекламы',
    '<i class="fa fa-check positive"></i>',
    '<i class="fa fa-check positive"></i>',
    '<i class="fa fa-check positive"></i>'
  ],
  [
    'Очередь обслуживания',
    '3',
    '2',
    '1'
  ],
  [
    'Скидки на услуги',
    '<i class="fa fa-close negative"></i>',
    '<i class="fa fa-check positive"></i>',
    '<i class="fa fa-check positive"></i>'
  ],
  [
    'Уменьшение процентов по задолжностям',
    '<i class="fa fa-close negative"></i>',
    'до 4%',
    'до 1%'
  ],
  [
    'Минимальные сроки работ',
    '<i class="fa fa-close negative"></i>',
    '<i class="fa fa-close negative"></i>',
    '<i class="fa fa-check positive"></i>'
  ],
  [
    'Цена за месяц',
    '49.99 рублей',
    '119.99 рублей',
    '249.99 рублей'
  ],
  [
    'Цена за год',
    '499.99 рублей',
    '1199.99 рублей',
    '2499.99 рублей'
  ]
]