window.footer = `
<div class="wrapper row4">
  <footer id="footer" class="hoc clear"> 
    <div class="one_quarter first">
      <h6 class="heading">Новости</h6>
      <ul class="nospace linklist">
        {{COMPANYNEWS}}
      </ul>
    </div>
    <div class="one_quarter">
     <h6 class="heading">Полезные источники</h6>
     <ul class="nospace linklist">
        <li>
          <article>
            <h2 class="nospace font-x1"><a>Мы советуем посетить вам сайты:</a></h2><br>
            <p class="nospace">Мы советуем посетить вам сайты:</p>
            <a class="nospace" target="_blank" href="http://ardronos.ru/?i=1">ardronos.ru</a><br>
            <a class="nospace" target="_blank" href="https://a-kurlaev.bitbucket.io/right-eating/">a-kurlaev.bitbucket.io/right-eating/</a><br>
            <a class="nospace" target="_blank" href="http://programms.hostronavt.ru">programms.hostronavt.ru</a>
          </article>
        </li>
      </ul>
    </div>
    <div class="one_quarter">
     <h6 class="heading">Наши партнёры</h6>
     <ul class="nospace linklist">
        <li>
          <article>
            <a target="_blank" href="#">
                <img src="${active == 0 ? '.' : window.header[window.active].robo != undefined ? '../..' : '..'}/images/partner/kurlaCompany.png" style="width: 150px;">
            </a>
          </article>
        </li>
      </ul>
      <div class="footer__plateg-container">
        <div class="footer__plateg"><div style="padding-bottom: 7px">Поддержите нас!</div><a class="footer__donate-button" href="${active == 0 ? './pages' : window.header[window.active].robo != undefined ? '..' : '.'}/donate.html">Пожертвовать</a></div>
        <div class="footer__plateg">Мы поддерживаем:<br>
          <a href="https://payeer.com" class="plateg-link"><img src="${active == 0 ? '.' : window.header[window.active].robo != undefined ? '../..' : '..'}/images/plateg/payeer.png"></a>
        </div>
      </div>
    </div>
    <div class="one_quarter">
      <h6 class="heading">Контакты</h6>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
          Новосибирск, Космическая 10
          </address>
        </li>
		<li><i class="fa fa-envelope-o"></i><a href="mailto:robotiXcom@yandex.ru" style="color: #A8A7A6;">robotiXcom@yandex.ru</a></li>
        <li><i class="fa fa-envelope-o"></i><a href="mailto:kurlaev.alex.linux@gmail.com" style="color: #A8A7A6;">kurlaev.alex.linux@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li><a target="_blank" class="faicon-vk" href="https://vk.com/public195027115"><i class="fa fa-vk"></i></a></li>
		<li><a target="_blank" class="faicon-telegram" href="https://t.me/RobotiXCompany"><i class="fa fa-telegram"></i></a></li>
      </ul>
    </div>
  </footer>
</div>
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <p class="fl_left">Новосибирск, 2020</a></p>
  </div>
</div>
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
`;



window.footerCompanyNewsTemplate = `
        <li>
          <article>
            <h2 class="nospace font-x1"><a>{{HEADER}}</a></h2>
            <time class="font-xs block btmspace-10" datetime="{{DATE}}">{{DATEASTEXT}}</time>
            <p class="nospace">{{TEXT}}</p>
          </article>
        </li>
`;