window.newsTemplate = `        <article class="one_quarter{{CLASSFIRST}}">
        <figure><a href="{{HREF}}"><img src="../images/uslugi/{{IMAGE}}" alt=""></a>
          
        </figure>
        <div class="txtwrap">
          <h4 class="heading">{{HEADER}}</h4>
          <p>{{CONTENT}}</p>
          <footer><a href="{{HREF}}">Подробнее >>></a></footer>
        </div>
      </article>`
