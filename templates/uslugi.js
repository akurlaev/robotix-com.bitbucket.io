window.news = [
    {
        href: 'sites.html',
        img: 'sites.jpg',
        cost: 'от 2',
        valute: 'дней',
        header: 'Сайты',
        content: 'Пишем сайты на заказ[…]',
        date: '9'
    },
	{
        href: 'u-ProgJs.html',
        img: 'js.png',
        cost: 'от 50',
        valute: 'руб',
        header: 'Java Script',
        content: 'Пишем программы на JS на заказ[…]',
        date: '2'
    },
	{
        href: 'u-ProgPython.html',
        img: 'python.png',
        cost: 'от 20',
        valute: 'руб',
        header: 'Python',
        content: 'Пишем программы на Python на заказ[…]',
        date: '1'
    },
    {
        href: 'u-Design.html',
        img: 'design.jpg',
        cost: 'от 1',
        valute: 'дня',
        header: 'Верстка сайта',
        content: 'Красивый дизайн и порядок на сайте[…]',
        date: '5'
    },
	{
        href: 'u-Chat-bot.html',
        img: 'chatbot.jpg',
        cost: 'от 2',
        valute: 'дней',
        header: 'Чат-бот',
        content: 'Чат-бот для ВК и других соц.сетей[…]',
        date: '8'
    },
	{
        href: 'u-Photoshop.html',
        img: 'photoshop.jpg',
        cost: 'от 50',
        valute: 'руб',
        header: 'Фоторедакция',
        content: 'Профессиональная фоторедакция[…]',
        date: '6'
    },
	{
        href: 'u-NodeJs.html',
        img: 'nodejs.png',
        cost: 'от 20',
        valute: 'руб',
        header: 'Сервер',
        content: 'Сервер на Node.js[…]',
        date: '4'
    },
    {
        href: 'u-Fonts.html',
        img: 'fonts.jpg',
        cost: 'от 1',
        valute: 'дня',
        header: 'Шрифты',
        content: 'Создадим для вас уникальный шрифт[…]',
        date: '7'
    },
    {
        href: 'u-Firebase.html',
        img: 'firebase.png',
        cost: 'от 1',
        valute: 'дня',
        header: 'Настройка Firebase',
        content: 'Настроим firebase под ваши нужды[…]',
        date: '3'
    }
];
