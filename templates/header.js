window.header = [
  { //index 0
    citata: ' — Ты всего лишь машина. Только имитация жизни. Робот сочинит симфонию? Робот превратит кусок холста в шедевр искусства? <br> — А Вы?',
    author: ' Я, робот (I, Robot)',
    image: 'images/backgrounds/home.png'
  },
  { //history 1
    robo: '',
    citata: '<span style="font-style: italic;">&#8220;Техника техникой, но лифт ломается чаще, чем лестница&#8221;</span>',
    author: 'Станислав Ежи Лец',
    image: '../../images/backgrounds/history.jpg'
  },
  { //shop 2
    citata: 'Марс — единственная планета, полностью населенная роботами (около 7 штук).',
    author: '',
    image: '../images/backgrounds/shop.jpg'
  },
  { //sites 3
    citata: '<span style="font-style: italic;">&#8220;Если отладка — процесс удаления ошибок, то программирование должно быть процессом их внесения&#8221;</span>',
    author: 'Эдсгер Вибе Дейкстра — голландский учёный',
    image: '../images/backgrounds/sites.jpg'
  },
  { //gallery 4
    citata: ' — Руки у тебя золотые!<br> — Нет, только контакты платиновые.',
    author: 'Приключения Электроника',
    image: '../images/backgrounds/gallery.png'
  },
  { //counter 5
    citata: '— Чёрт, мужик, не умирай! Ты всё ещё должен мне деньги!',
    author: 'Цитата из игры «Too Human»',
    image: '../images/backgrounds/counter.jpg'
  },
  { //about 6
    citata: '<span style="font-style: italic;">&#8220;Рекурсия — основа программирования, поскольку она сокращает время написания программы&#8221;</span>',
    author: 'Алан Перлис — американский учёный',
    image: '../images/backgrounds/about.jpg'
  },
  { //uslugi 7
    citata: '<span style="font-style: italic;">&#8220;Нельзя научиться программированию с помощью ручного калькулятора, но можно забыть арифметику&#8221;</span>',
    author: 'Алан Перлис — американский учёный',
    image: '../images/backgrounds/uslugi.jpg'
  },
  { //donate 8
    citata: '<span style="font-style: italic;">&#8220;Милостыня – косвенный налог на право иметь хоть немного совести&#8221;</span>',
    author: 'Оскар Боэций',
    image: '../images/backgrounds/donate.jpg'
  },
  { //locked 9
    citata: 'Кстати о птичках, этот сайт заблокирован',
    author: 'Компания RobotiX',
    image: '../images/backgrounds/locked.jpg'
  },
  { //robototehnic/news 10
    robo: '',
    citata: ' — Ты всего лишь машина. Только имитация жизни. Робот сочинит симфонию? Робот превратит кусок холста в шедевр искусства? <br> — А Вы?',
    author: ' Я, робот (I, Robot)',
    image: '../../images/backgrounds/home.png'
  },
  { //premium 11
    citata: ' — Гони деньги! <br> — А Вы мне что? <br> — Ну... Я... А я вам месяц без рекламы на сайте',
    author: ' Игорь Кубакин — основатель компании RobotiX',
    image: '../images/backgrounds/premium.png'
  },
  { //article 12
    robo: '',
    citata: ' — Вы наверно счастливы! Вы же играете в Minecraft на работе! <br> — Я не играю, я только пишу статьи по рассказам программистов!',
    author: ' Пользователь, Себастьян Торрес — автор статей',
    image: '../../images/backgrounds/articleMine.webp'
  },
  { //articles 13
    citata: '<span style="font-style: italic;">&#8220;Всё самое новое считается самым интересным&#8221;</span>',
    author: 'Себастьян Торрес — автор статей',
    image: '../images/backgrounds/articleMine.webp'
  },
  { //errors 14
    citata: 'Лекарство от глупости ещё не найдено, так же как и эта страница',
    author: 'Компания RobotiX',
    image: '../images/backgrounds/404.jpg'
  },
  { //indevelop 15
    citata: '<span style="font-style: italic;">&#8220;Если вашего бизнеса до сих пор нет в Интернете, значит, скоро у вас не будет бизнеса&#8221;</span>',
    author: 'Билл Гейтс',
    image: '../images/backgrounds/indevelop.jpg'
  },
  { //fonts 16
    citata: '<span style="font-style: italic;">&#8220;Нам никогда не сделать красивой кириллицу, потому что она почти наполовину состоит из неинтересных, вялых по конструкции, похожих друг на друга как спины в очереди, знаков&#8221;</span>',
    author: 'Юрий Гордон',
    image: '../images/backgrounds/fonts.jpg'
  },
  { //chat-bot 17
    citata: '<span style="font-style: italic;">&#8220;Люди могут часами переписываться с чат-ботом и не подозревать, что по ту сторону экрана находится ИИ.&#8221;</span>',
    author: 'Опубликованные научные данные',
    image: '../images/backgrounds/chat-bot.jpg'
  },
  { //some tovar 18
    robo: '',
    citata: '<span style="font-style: italic;">&#8220;Фраза: "Пойдём, что-нибудь тебе купим" - имеет сильное лечебное свойство&#8221;</span>',
    author: 'Британские учёные',
    image: '../../images/backgrounds/shop.jpg'
  },
  { //progPython 19
    citata: '<span style="font-style: italic;">&#8220;Есть всего два типа языков программирования: те, на которые люди всё время ругаются, и те, которые никто не использует.&#8221;</span>',
    author: 'Bjarne Stroustrup',
    image: '../images/backgrounds/python.jpg'
  },
  { //holidays 20
    citata: '<span style="font-style: italic;">&#8220;Если вы хотите, чтобы и на вашей улице был праздник, поселитесь на той улице, где уже есть праздник.&#8221;</span>',
    author: 'Ефим Шпигель',
    image: '../images/backgrounds/holiday.jpeg'
  },
  { //company news 21
    citata: '<span style="font-style: italic;">&#8220;Газета - первый черновик истории.&#8221;</span>',
    author: 'Филип Грэм',
    image: '../images/backgrounds/company.jpg'
  },
  { //historyPrograming 22
    robo: '',
    citata: '<span style="font-style: italic;">&#8220;Не волнуйся, если не работает. Если бы все всегда работало, у тебя бы не было работы.&#8221;</span>',
    author: '',
    image: '../../images/backgrounds/historyProgramming.jpg'
  },
  { //historyProgramer 23
    robo: '',
    citata: '<span style="font-style: italic;">&#8220;Человек может быть гением или обладать всеми необходимыми навыками, но если он не верит в себя, он не будет выкладываться по полной.&#8221;</span>',
    author: 'Марк Цукерберг',
    image: '../../images/backgrounds/historyProgrammer.png'
  },
  { //good-programs 24
    robo: '',
    citata: '<span style="font-style: italic;">&#8220;Программное обеспечение подчиняется закону расширения газов: оно стремится заполнить всю имеющуюся память.&#8221;</span>',
    author: 'Ларри Глизон',
    image: '../../images/backgrounds/good-programs.jpg'
  },
  { // citaty 25
    citata: '<span style="font-style: italic;">&#8220;Проблема цитат в сети Интернет заключается в том, что люди безоговорочно верят в их подлинность&#8221;</span>',
    author: 'Владимир Ильич Ленин',
    image: '../images/backgrounds/citaty.jpg'
  }
]
