window.headerTemplate = `<div class="bgded" style="background-image:url('{{IMAGE}}');"> 
                          <div class="wrapper row1">
                            <header id="header" class="hoc clear"> 
                              <div id="logo" class="fl_left">
                                <h1><a href="{{PASSTOMAIN}}/index.html">RobotiX</a></h1>
                                <button class="header__links-button">
                                  <span class="header__links-button_icon"></span>
                                  <span class="header__links-button_icon"></span>
                                  <span class="header__links-button_icon"></span>
                                </button>
                              </div>
                              <nav class="mainav fl_right">
                                <ul class="clear">
                                  <li><a class="0" href="{{PASSTOMAIN}}/index.html">Главная</a></li>
                                  <li><a class="drop">Услуги</a>
                                    <ul>
                                      <li><a class="7" href="{{PASSTOPAGES}}/uslugi.html">Все услуги</a></li>
                                      <li><a class="3" href="{{PASSTOPAGES}}/sites.html">Сайты</a></li>
                                      <li><a class="16" href="{{PASSTOPAGES}}/u-Fonts.html">Шрифты</a></li>
                                      <li><a class="17" href="{{PASSTOPAGES}}/u-Chat-bot.html">Чат-бот</a></li>
                                    </ul>
                                  </li>
                                  <li><a class="2 18" href="{{PASSTOPAGES}}/shop.html">Магазин</a></li>
                                  <li><a class="drop">Разделы</a>
                                    <ul>
                                      <li class="drop"><a>Бета-версии</a>
                                        <ul>
                                          <li><a class="beta_1" href="{{PASSTOPAGES}}/table.html">Создание таблиц онлайн</a></li>
                                        </ul>
                                      </li>
                                      <li class="drop"><a>Программирование</a>
                                        <ul>
                                          <li><a class="22" href="{{PASSTOPAGES}}/programming/historyProgramming.html"><span>История</span> <span>программирования</span></a></li>
                                          <li><a class="23" href="{{PASSTOPAGES}}/programming/historyProgrammer.html"><span>Успешные</span> <span>программисты</span></a></li>
                                          <li><a class="24" href="{{PASSTOPAGES}}/programming/good-programs.html"><span>Полезные</span> <span>программы</span></a></li>
                                        </ul>
                                      </li>
                                      <li class="drop"><a>Робототехника</a>
                                        <ul>
                                          <li><a class="1" href="{{PASSTOPAGES}}/robototehnic/history.html"><span>История</span> <span>робототехники</span></a></li>
                                          <li><a class="10" href="{{PASSTOPAGES}}/robototehnic/news.html">Новости</a></li>
                                        </ul>
                                      </li>
                                      <li class="drop"><a>Разный контент</a>
                                        <ul>
                                          <li><a class="13" href="{{PASSTOPAGES}}/articles.html">Статьи</a></li>
                                        </ul>
                                      </li>
                                    </ul>
                                  </li>
                                  <li><a class="drop">О нас</a>
                                    <ul>
                                      <li><a class="6" href="{{PASSTOPAGES}}/about.html">О нас</a></li>
                                      <li><a class="25" href="{{PASSTOPAGES}}/citaty.html">Цитаты</a></li>
                                      <li><a class="11" href="{{PASSTOPAGES}}/premium.html">RobotiX Premium</a></li>
                                      <li><a class="8" href="{{PASSTOPAGES}}/donate.html">Пожертвования</a></li>
                                      <li><a class="4" href="{{PASSTOPAGES}}/gallery.html">Галерея</a></li>
<!--                                      <li><a class="20" href="{{PASSTOPAGES}}/holidays.html">Праздники</a></li>-->
                                      <li><a class="21" href="{{PASSTOPAGES}}/company-news.html">Новости компании</a></li>
                                    </ul>
                                  </li>
                                </ul>
                              </nav>
                              
                            </header>
                            <nav class="mainav menu-wrapper">
                              <div id="menu">
                                  <ul class="menu__list">
                                    <li class="menu__link 0"><a href="{{PASSTOMAIN}}/index.html">Главная</a></li>
                                  <li class="menu__link"><a class="drop">Услуги</a>
                                    <ul class="menu__list_list">
                                      <li class="7 menu__link"><a href="{{PASSTOPAGES}}/uslugi.html">Все услуги</a></li>
                                      <li class="3 menu__link"><a href="{{PASSTOPAGES}}/sites.html">Сайты</a></li>
                                      <li class="16 menu__link"><a href="{{PASSTOPAGES}}/u-Fonts.html">Шрифты</a></li>
                                      <li class="17 menu__link"><a href="{{PASSTOPAGES}}/u-Chat-bot.html">Чат-бот</a></li>
                                    </ul>
                                  </li>
                                  <li class="menu__link 2 18"><a href="{{PASSTOPAGES}}/shop.html">Магазин</a></li>
                                  <li class="menu__link"><a class="drop">Разделы</a>
                                    <ul class="menu__list_list">
                                    <li class="menu__link drop"><a>Бета-версии</a>
                                        <ul class="menu__list_list">
                                          <li><a class="beta_1" href="{{PASSTOPAGES}}/table.html">Создание таблиц онлайн</a></li>
                                        </ul>
                                      </li>
                                      <li class="menu__link drop"><a>Программирование</a>
                                        <ul class="menu__list_list">
                                          <li><a class="22" href="{{PASSTOPAGES}}/programming/historyProgramming.html"><span>История</span> <span>программирования</span></a></li>
                                          <li><a class="23" href="{{PASSTOPAGES}}/programming/historyProgrammer.html"><span>Успешные</span> <span>программисты</span></a></li>
                                          <li><a class="24" href="{{PASSTOPAGES}}/programming/good-programs.html"><span>Полезные</span> <span>программы</span></a></li>
                                        </ul>
                                      </li>
                                      <li class="menu__link drop"><a>Робототехника</a>
                                        <ul class="menu__list_list">
                                          <li><a class="1" href="{{PASSTOPAGES}}/robototehnic/history.html"><span>История</span> <span>робототехники</span></a></li>
                                          <li><a class="10" href="{{PASSTOPAGES}}/robototehnic/news.html">Новости</a></li>
                                        </ul>
                                      </li>
                                      <li class="menu__link drop"><a>Разный контент</a>
                                        <ul class="menu__list_list">
                                          <li><a class="13" href="{{PASSTOPAGES}}/articles.html">Статьи</a></li>
                                        </ul>
                                      </li>
                                    </ul>
                                  </li>
                                  <li class="menu__link"><a class="drop">О нас</a>
                                    <ul class="menu__list_list">
                                      <li class="6 menu__link"><a href="{{PASSTOPAGES}}/about.html">О нас</a></li>
                                      <li class="25 menu__link"><a href="{{PASSTOPAGES}}/citaty.html">Цитаты</a></li>
                                      <li class="8 menu__link"><a href="{{PASSTOPAGES}}/donate.html">Пожертвовать</a></li>
                                      <li class="11 menu__link"><a href="{{PASSTOPAGES}}/premium.html">RobotiX Premium</a></li>
                                      <li class="4 menu__link"><a href="{{PASSTOPAGES}}/gallery.html">Галерея</a></li>
<!--                                      <li class="20 menu__link"><a href="{{PASSTOPAGES}}/holidays.html">Праздники</a></li>-->
                                      <li class="21 menu__link"><a href="{{PASSTOPAGES}}/company-news.html">Новости компании</a></li>
                                    </ul>
                                  </li>
                                  </ul>
                                </div>
                                </nav>
                          </div>
                          <div class="wrapper">
                            <article id="pageintro" class="hoc clear"> 
                              <div class="transbox">
                                <p>{{CITATA}}<div align="right">{{AUTHOR}}</div></p>
                              </div>
                              <footer><a class="btn" href="{{PASSTOPAGES}}/robototehnic/history.html#citaty">Больше >>></a></footer>
                            </article>
                          </div>
                        </div>`
