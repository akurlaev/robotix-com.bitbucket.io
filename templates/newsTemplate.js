window.newsTemplate = `        <article class="one_third{{CLASSFIRST}}">
        <figure><a href="{{HREF}}" target="_blank"><img src="${active == 0 ? '' : window.header[window.active].robo != undefined ? '../../' : '../'}images/news/{{IMAGE}}" alt=""></a>
          <figcaption>
            <time><strong>{{DAY}}</strong><em>{{MONTH}}</em></time>
          </figcaption>
        </figure>
        <div class="txtwrap">
          <h4 class="heading">{{HEADER}}</h4>
          <p>{{CONTENT}}</p>
          <footer><a href="{{HREF}}" target="_blank">Больше »</a></footer>
        </div>
      </article>`
