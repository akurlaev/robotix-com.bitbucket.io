window.companyNews = [
    {
        href: 'update-2020.12.25',
        img: 'update.25.12.2020.png',
        header: 'Большое обновление!',
        content: 'Новый контент, новые функции и многое другое!',
        date: '2020-12-25',
        dateAsText: '25 декабря 2020',
        video: false,
        picture: false,
        news: true,
        category: ['Наша компания'],
        display: ''
    },
    {
        header: 'Переезд',
           date: '2020-10-25',
        dateAsText: '25 Октября 2020',
        video: false,
        picture: false,
        news: true,
        content: 'Наш сайт переехал на новый хостинг! Теперь наш адресс <span style="width: max-content; display: inline-block;">robotix-com.web.app!',
        href: 'pereezd',
        img: 'pereezd.jpg',
        category: ['Наша компания'],
        display: ''
    },
    {
        header: 'Обновление Главной',
        date: '2021-06-13',
        dateAsText: '13 Июня 2021',
        video: false,
        picture: false,
        news: true,
        content: 'Обновление Главной и добавление одной очень важной статьи',
        href: 'update-2021.06.13',
        img: 'update.13.06.2021.png',
        category: ['Наша компания'],
        display: ''
    },
    {
        href: 'robotixLogo',
        img: 'robotixLink.jpg',
        header: 'Новый логотип',
        content: 'Встречайте новый логотип нашей компании!',
        date: '2020-09-10',
        dateAsText: '9 Сентября 2020',
        video: false,
        picture: false,
        news: true,
        category: ['Наша компания'],
        display: ''
    },
    {
        header: 'Запуск сайта',
        date: '2020-04-30',
        dateAsText: '30 Апреля 2020',
        video: false,
        picture: false,
        news: true,
        content: 'Сайт robotix.hostronavt.ru появился в интернете! На данный момент доступен не весь функционал этого ресурса.',
        href: 'start-site',
        img: 'start-site.jpg',
        category: ['Наша компания'],
        display: ''
    }/*,
    {
        header: 'Electron Cell Sandbox',
        date: '2021-06-13',
        dateAsText: '13 Июня 2021',
        video: false,
        picture: false,
        news: true,
        content: 'Ведущий разработчик компании RobotiX заявил о создании продукта Electron Cell Sandbox community edition',
        href: 'start-ECS',
        img: 'ecs.png',
        category: ['Наша компания'],
        display: ''
    },
    {
        header: 'Ответ Mister Master-а',
        date: '2021-06-14',
        dateAsText: '14 Июня 2021',
        video: false,
        picture: false,
        news: true,
        content: 'Разработчик Electron Cell Sandbox official прокомментировал заявление ведущего разработчика компании RobotiX',
        href: 'Mister-Master-answer',
        img: 'ecs.png',
        category: ['Наша компания'],
        display: ''
    }*/
];

if (window.active == 21 || window.active == 13 || window.active == 0) {
    let articles = [
        {
            header: 'Запуск сайта',
            content: 'Сайт robotix.hostronavt.ru появился в интернете! На данный момент доступен не весь функционал этого ресурса.',
            date: '30.4.2020',
            id: 'start-site'
        },
        {
            header: 'Переезд',
            content: '30 апреля 2020 года мы запустили сайт на бесплатном хостинге <a target="_blank" href="https://hostronavt.ru">Hostronavt</a> который был доступен <a target="_blank" href="http://robotix.hostronavt.ru">здесь</a>. Этот сайт был создан для размещения актуальной информации о робототехнике. По адресу http://robotix.hostronavt.ru осталась старая версия сайта, которую мы не будем обновлять. ' +
                'Сейчас наш сайт стал официальным сайтом нашей компании и переехал на хостинг <a target="_blank" href="https://firebase.google.com">firebase</a>. Сейчас вы можете найти на нашем сайте информацию о робототехнике, программировании, нашей компании, наших услугах и красивые картинки.',
            date: '25.10.2020',
            id: 'pereezd'
        },
        {
            header: 'Большое обновление!',
            content: `На нашем сайте вышло большое обновление!<br> Вот что в него вошло: 
                <ul>
                    <li>Новые страницы
                        <ul>
                            <li><a href="./programming/historyProgramming.html">История программирования</a></li>
                            <li><a href="./programming/historyProgrammer.html">Успешные программисты</a></li>
                            <li><a href="./programming/good-programs.html">Полезные программы</a></li>
<!--                            <li>Наши программы</li>-->
                            <li><a href="./company-news.html">Новости компании</a></li>
<!--                            <li>Раздел история праздников</li>-->
                        </ul>
                    </li>
                    <li>Новые функции
                        <ul>
                            <li><a href="./articles.html">Поиск статей</a></li>
                            <li><a href="./articles.html">Возможность делать ссылку на конкретную статью</a></li>
                            <li><a href="./articles.html">Включение и исключение из поиска определённого типа статей</a></li>
                            <li><span style="cursor: pointer; color: var(--company-color)" onclick="openSettingsMenu()">Выбор темы</span></li>
<!--                            <li>Поиск по сайту</li>-->
                        </ul>
                    </li>
                    <li>Новые темы
                        <ul>
                            <li>Новогодняя тема</li>
                        </ul>
                    </li>
                    <li>Новый контент
                        <ul>
                            <li><a href="articles.html?artId=eco">Новый арт от Михаила Сизова на тему экология</a></li>
                        </ul>
                    </li>
                </ul>`,
            date: '25.12.2020',
            id: 'update-2020.12.25'
        },
        {
            header: 'Новый логотип',
            content: 'Здравствуйте, официальный сайт RobotiX Company представляет новый логотип!<br><br><img src="../images/articles/robotix.jpg"/>',
            date: '10.9.2020',
            id: 'robotixLogo'
        },
        {
            header: 'Обновление Главной',
            content: `Сегодня мы провели небольшое обновление, вот основные его пункты:
                <ul>
                    <li><a href="../old-index.html">Главная</a></li>
                    <li><span style="cursor: pointer; color: var(--company-color)" onclick="openSettingsMenu()">Тёмная тема</span></li>
<!--                    <li><a style="cursor: pointer;" onclick="linkClick('start-ECS')">Ведущий разработчик компании RobotiX заявил о создании продукта Electron Cell Sandbox community edition</a></li>-->
                </ul>
            `,
            date: '13.06.2021',
            id: 'update-2021.06.13'
        },
        {
            header: 'Electron Cell Sandbox',
            content: `Сегодня ведущий разработчик компании RobotiX заявил о создании продукта Electron Cell Sandbox community edition.
            <br>
            <p><i style="max-width: 700px; display: block;">&#8220;Мы уже долгое время ведём переговоры
            с Mister Master о создании общего продукта (Electron Cell Sandbox) из 2 ветвей развития,
            но пока безрезультатно. Именно поэтому я принял решение создать новый продукт из 
            своей ветви - Electron Cell Sandbox community edition, он имеет расширенный функционал,
            а также является кроссплатформенным в отличии от главной ветви.&#8221;</i><span style="text-align: right; display: block;">
             — Курлаев Александр, ведущий разработчик компании RobotiX</span></p>
            <br>
            <p>Давайте же разберёмся, что же такое ECS?
            <br>
            <br>
            
            <i style="max-width: 700px; display: block;">&#8220;Добро пожаловать в игру 
            Electron Cells Sandbox. Здесь ты можешь создать различные структуры, логические цепочки
            и даже целый компьютер. Функционал в данной программе позволит вам создавать все что 
            угодно в рамках клеточного автомата WireWorld а также модификаций автомата WireWorld
            v1/WireWorld v2. Вот правила клеточного автомата <a href="https://ru.wikipedia.org/wiki/Wireworld">https://ru.wikipedia.org/wiki/Wireworld</a>
            (советую также посмотреть видео по этой ссылке https://www.youtube.com/watch?v=yOUh7OjWyHs).
            Также вы можете поменять параметры цветов и рабочего окна в файле settings.json&#8221;  —  </i>
            <br>
            
            сказано в файле-руководстве к продукту. По сути это среда для создания виртуальных электронных
            логических элементов или просто изображений.
            </p>
            <p>
            Разберём основные отличия ECS official и и community edition: <br>
            <ul>
                <li>поддерживаемые форматы: ECS official поддерживает только свой формат ".ecs",
                а ECS community edition помимо этого поддерживает устаревший ".ecsd", облегчённый ".ecsl" и зашифрованные ".ecse" и ".ecsel"</li>
                <li>кроссплатформенность: ECS official выпускается исключительно под Windows 8.1+,
                 а ECS community edition гарантираванно поддерживает Linux(Arch/+Wine) и Windows 7+ (на остальных ОС не тестировалось)</li>
                <li>функции: здесь отличий крайне мало, можно отметить масштабирование с помощью клавиш и более удобное перемещение поля у ECS community edition</li>
            </ul>
            </p>
            <br>
            <p>
            Из этого можно сделать вывод, что ECS community edition лучше чем ECS official. Скачать обе версии вы можете <a download href="../docs/Electron Cells Sandbox.zip">здесь</a>.<br>
            Если вы в чём-то не согласны с этой статьёй или вы хотите её дополнить, пишите в комментарии! 
            </p>
            <div id="ue-embedded-widget"></div>
            <script type='text/javascript'>

                var _ues = {
                    host:'robotix.userecho.com',
                    forum:'1',
                    lang:'ru',
                    chat:{channel:null},
                    chat_tab_show:false,
                    tab_show:false,
                    container_id:'ue-embedded-widget',
                };

                (function() {
                    var _ue = document.createElement('script'); _ue.type = 'text/javascript'; _ue.async = true;
                    _ue.src = 'https://cdn.userecho.com/js/widget-1.4.gz.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(_ue, s);
                })();

            </script>
            <p>Если там^ нет окна отзывов, перейдите по ссылке: <a href="https://robotix.userecho.com/ru/communities/1/topics/2-electron-cell-sandbox">https://robotix.userecho.com/ru/communities/1/topics/2-electron-cell-sandbox</a></p>
            `,
            date: '13.06.2021',
            id: 'start-ECS'
        },
        {
            header: 'Ответ Mister Master-а',
            content: `<p><a style="cursor: pointer;" onclick="linkClick('start-ECS')">История</a>, опубликованная на нашем сайте вчера, получила продолжение: сегодня разработчик Electron Cell Sandbox official
            прокомментировал заявление ведущего разработчика компании RobotiX.</p>
            <p><i style="max-width: 700px; display: block;">&#8220;Эта ситуация напоминает отношения разрабов модов и разрабов Майна. Mojang делает
            продукт доступным для понимания всем юзерам, а моды добавляют новый функционал в игру(иногда перебарщивая). Так что я делаю продукт 
            доступным для всех чайников (обычных пользователей - примечание редакции), а если им надо добавить криптографическое шифрование в файлы,
            то они сами этого сделают, а остальные предпочтут не заниматься этим&#8221;</i><span style="text-align: right; display: block;">
             — заявил Mister Master, разработчик Electron Cell Sandbox official</span></p>
            <br>
            <p>
            После этого Александр Курлаев заявил: <i style="max-width: 700px; display: block;">&#8220;Возможно некоторые функции для обычного пользователя
            будут действительно лишними, но разве может быть ненужным формат файла, занимающий в 7-8 раз меньше памяти? И разве масштабирование с помощью
            клавиш лишнее? Или надо выбросить удобное, интуитивно понятное перетаскивание поля, заменив его перетаскиванием, при котором поле "отстаёт" от
            курсора? Нет. Именно поэтому я создаю Electron Cell Sandbox community edition с полным функционалом, но раз уж Mister Master всё же решил делать
            продукт с маленьким функционалом, я так и быть сделаю модули для модификации официальной версии.&#8221;</i>
            </p>
            `,
            date: '14.06.2021',
            id: 'Mister-Master-answer'
        }
    ]
    if (window.active == 21 || window.active == 0) {
        window.articlesLinks = window.companyNews;
        window.articles = articles;
    }
    else {
        window.articlesLinks = window.articlesLinks.concat(window.companyNews);
        window.articles = window.articles.concat(articles);
    }
}

