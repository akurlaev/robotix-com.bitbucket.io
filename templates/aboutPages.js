window.aboutPages = [
  {
    link: '../old-index.html',
    header: 'Главная',
    description: 'на этой странице будут публиковаться новости нашей компании.'
  },
  {
    link: 'uslugi.html',
    header: 'Все услуги',
    description: 'на этой странице вы можете выбрать необходимую вам услугу.'
  },
  {
    link: 'sites.html',
    header: 'Сайты',
    description: 'здесь вы можете ознакомиться с деталеми нашей работы и заказать сайт'
  },
  {
    link: 'shop.html',
    header: 'Магазин',
    description: 'интернет магазин от нашей компании (на данный момент он ещё пустой).'
  },
  {
    link: 'robototehnic/history.html',
    header: 'История робототехники',
    description: 'на этой странице вы можете ознакомится с историей робототехники. Также здесь располагаются цитаты великих людей, использованные на нашем сайте.'
  },
  {
    link: 'robototehnic/news.html',
    header: 'Новости робототехники',
    description: 'на этой странице вы найдёте самые интересные новости робототехники.'
  },
  {
    link: 'donate.html',
    header: 'Пожертвовать',
    description: 'здесь вы можете поддержать нашу компанию.'
  },
  {
    link: 'gallery.html',
    header: 'Галерея',
    description: 'тут находятся наши фотографии.'
  },
  {
    link: 'premium.html',
    header: 'RobotiX Premium',
    description: 'на этой странице вы можете купить RobotiX Premium.'
  },
  {
    link: 'programming/historyProgramming.html',
    header: 'История программирования',
    description: 'на этой странице вы можете ознакомится с историей программирования.'
  },
  {
    link: 'programming/historyProgrammer.html',
    header: 'Успешные программисты',
    description: 'здесь вы найдёте информацию об успешных программистах.'
  },
  {
    link: 'programming/good-programs.html',
    header: 'Полезные программы',
    description: 'описание с плюсами и минусами редакторов кода.'
  },
  {
    link: 'company-news.html',
    header: 'Новости компании',
    description: 'здесь вы найдёте новости нашей компании.'
  },
  {
    link: 'holidays.html',
    header: 'Праздники',
    description: 'здесь вы найдёте список поддерживаемых нами праздников.'
  }
]