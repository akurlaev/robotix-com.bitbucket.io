window.gallaryTemplate = `
    <li class="one_quarter {{CLASSFIRST}}" onclick="openImage('{{SRC}}', '{{HEADER}}')">
        <div class="img" style="background-image: url('{{SRC}}')">
            <a>
                <img style="visibility: hidden" src="https://firebasestorage.googleapis.com/v0/b/robotix-com.appspot.com/o/images%2Fgallery%2F01.png?alt=media&token=0af54878-e691-4056-a3ec-70e9c2aa428b">
            </a>
            <div class="img-title">{{HEADER}}</div>
            <div class="date">{{DATEFORUSER}}</div>
        </div>
        <div class="header">{{HEADER}}</div>
    </li>
`