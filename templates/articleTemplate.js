window.articleLinkTemplate = `        <article class="one_third article-link{{CLASSFIRST}}" onclick="linkClick('{{HREF}}')">
        <figure><a target="_blank"><img src="{{IMAGE}}" alt=""></a>
          <figcaption>
            <time><strong style="padding: 12px 0;"><i class="fa {{CLASSARTORVID}}"></i></strong></time>
          </figcaption>
        </figure>
        <div class="txtwrap">
          <h4 class="heading">{{HEADER}}</h4>
          <p>{{CONTENT}}</p>
          <footer><a target="_blank">Больше »</a></footer>
        </div>
      </article>`

window.articleTemplate = `<div class="article none" id="{{ID}}">
  <h3>{{HEADER}}</h3>
  <time>{{DATE}}</time>
  <div class="article__content">{{CONTENT}}</div>
</div>`