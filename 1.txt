<body id="top" data-new-gr-c-s-check-loaded="8.871.0" data-gr-ext-installed="" class="">
<!-- Top Background Image Wrapper -->
<script>window.active = 0;window.newsRowAmount = 1;window.articlesContainersSeveralMode = true/*search there is impossible*/;window.articlesOnlyLinks = true;</script>
<div class="bgded"><div class="bgded" style="background-image:url('images/backgrounds/home.png');">
                          <div class="wrapper row1" style="background: #000b;">
                            <header id="header" class="hoc clear">
                              <div id="logo" class="fl_left">
                                <h1><a href="./index.html">RobotiX</a></h1>
                                <button class="header__links-button">
                                  <span class="header__links-button_icon"></span>
                                  <span class="header__links-button_icon"></span>
                                  <span class="header__links-button_icon"></span>
                                </button>
                              </div>
                              <nav class="mainav fl_right">
                                <ul class="clear">
                                  <li><a class="0 active" href="./index.html">Главная</a></li>
                                  <li><a class="drop">Услуги</a>
                                    <ul>
                                      <li><a class="7" href="./pages/uslugi.html">Все услуги</a></li>
                                      <li><a class="3" href="./pages/sites.html">Сайты</a></li>
                                      <li><a class="16" href="./pages/u-Fonts.html">Шрифты</a></li>
                                      <li><a class="17" href="./pages/u-Chat-bot.html">Чат-бот</a></li>
                                    </ul>
                                  </li>
                                  <li><a class="2 18" href="./pages/shop.html">Магазин</a></li>
                                  <li><a class="drop">Разделы</a>
                                    <ul>
                                      <li class="drop"><a>Бета-версии</a>
                                        <ul>
                                          <li><a class="13" href="./pages/articles.html">Статьи</a></li>
                                          <li><a class="beta_1" href="./pages/table.html">Создание таблиц онлайн</a></li>
                                        </ul>
                                      </li>
                                      <li class="drop"><a>Программирование</a>
                                        <ul>
                                          <li><a class="22" href="./pages/programming/historyProgramming.html"><span>История</span> <span>программирования</span></a></li>
                                          <li><a class="23" href="./pages/programming/historyProgrammer.html"><span>Успешные</span> <span>программисты</span></a></li>
                                          <li><a class="24" href="./pages/programming/good-programs.html"><span>Полезные</span> <span>программы</span></a></li>
                                        </ul>
                                      </li>
                                      <li class="drop"><a>Робототехника</a>
                                        <ul>
                                          <li><a class="1" href="./pages/robototehnic/history.html"><span>История</span> <span>робототехники</span></a></li>
                                          <li><a class="10" href="./pages/robototehnic/news.html">Новости</a></li>
                                        </ul>
                                      </li>
                                    </ul>
                                  </li>
                                  <li><a class="drop">О нас</a>
                                    <ul>
                                      <li><a class="6" href="./pages/about.html">О нас</a></li>
                                      <li><a class="11" href="./pages/premium.html">RobotiX Premium</a></li>
                                      <li><a class="8" href="./pages/donate.html">Пожертвования</a></li>
                                      <li><a class="4" href="./pages/gallery.html">Галерея</a></li>
                                      <li><a class="20" href="./pages/holidays.html">Праздники</a></li>
                                      <li><a class="21" href="./pages/company-news.html">Новости компании</a></li>
                                    </ul>
                                  </li>
                                </ul>
                              </nav>

                            </header>
                            <nav class="mainav menu-wrapper">
                              <div id="menu">
                                  <ul class="menu__list">
                                    <li class="menu__link 0 active"><a href="./index.html">Главная</a></li>
                                  <li class="menu__link"><a class="drop">Услуги</a>
                                    <ul class="menu__list_list">
                                      <li class="7 menu__link"><a href="./pages/uslugi.html">Все услуги</a></li>
                                      <li class="3 menu__link"><a href="./pages/sites.html">Сайты</a></li>
                                      <li class="16 menu__link"><a href="./pages/u-Fonts.html">Шрифты</a></li>
                                      <li class="17 menu__link"><a href="./pages/u-Chat-bot.html">Чат-бот</a></li>
                                    </ul>
                                  </li>
                                  <li class="menu__link 2 18"><a href="./pages/shop.html">Магазин</a></li>
                                  <li class="menu__link"><a class="drop">Разделы</a>
                                    <ul class="menu__list_list">
                                    <li class="menu__link drop"><a>Бета-версии</a>
                                        <ul class="menu__list_list">
                                          <li><a class="13" href="./pages/articles.html">Статьи</a></li>
                                          <li><a class="beta_1" href="./pages/table.html">Создание таблиц онлайн</a></li>
                                        </ul>
                                      </li>
                                      <li class="menu__link drop"><a>Программирование</a>
                                        <ul class="menu__list_list">
                                          <li><a class="22" href="./pages/programming/historyProgramming.html"><span>История</span> <span>программирования</span></a></li>
                                          <li><a class="23" href="./pages/programming/historyProgrammer.html"><span>Успешные</span> <span>программисты</span></a></li>
                                          <li><a class="24" href="./pages/programming/good-programs.html"><span>Полезные</span> <span>программы</span></a></li>
                                        </ul>
                                      </li>
                                      <li class="menu__link drop"><a>Робототехника</a>
                                        <ul class="menu__list_list">
                                          <li><a class="1" href="./pages/robototehnic/history.html"><span>История</span> <span>робототехники</span></a></li>
                                          <li><a class="10" href="./pages/robototehnic/news.html">Новости</a></li>
                                        </ul>
                                      </li>
                                    </ul>
                                  </li>
                                  <li class="menu__link"><a class="drop">О нас</a>
                                    <ul class="menu__list_list">
                                      <li class="6 menu__link"><a href="./pages/about.html">О нас</a></li>
                                      <li class="8 menu__link"><a href="./pages/donate.html">Пожертвовать</a></li>
                                      <li class="11 menu__link"><a href="./pages/premium.html">RobotiX Premium</a></li>
                                      <li class="4 menu__link"><a href="./pages/gallery.html">Галерея</a></li>
                                      <li class="20 menu__link"><a href="./pages/holidays.html">Праздники</a></li>
                                      <li class="21 menu__link"><a href="./pages/company-news.html">Новости компании</a></li>
                                    </ul>
                                  </li>
                                  </ul>
                                </div>
                                </nav>
                          </div>
                          <div class="wrapper">
                            <article id="pageintro" class="hoc clear">
                              <div class="transbox">
                                <p> — Ты всего лишь машина. Только имитация жизни. Робот сочинит симфонию? Робот превратит кусок холста в шедевр искусства? <br> — А Вы?</p><div align="right"> Я, робот (I, Robot)</div><p></p>
                              </div>
                              <footer><a class="btn" href="./pages/robototehnic/history.html#citaty">Больше &gt;&gt;&gt;</a></footer>
                            </article>
                          </div>
                        </div></div>
<!--News-->
<div class="wrapper row3" style="background: #111;">
  <section class="hoc container clear">
    <div id="title" class="sectiontitle">
      <h6 class="heading">Сайт компании Robotix</h6>
      <p>На этой странице собраны части некоторых страниц нашего сайта</p>
    </div>
    <div class="ads-container">
      <!-- Yandex.RTB R-A-659817-2 -->
      <div id="yandex_rtb_R-A-659817-2"></div>
      <script type="text/javascript">
        (function(w, d, n, s, t) {
          w[n] = w[n] || [];
          w[n].push(function() {
            Ya.Context.AdvManager.render({
              blockId: "R-A-659817-2",
              renderTo: "yandex_rtb_R-A-659817-2",
              async: true
            });
          });
          t = d.getElementsByTagName("script")[0];
          s = d.createElement("script");
          s.type = "text/javascript";
          s.src = "//an.yandex.ru/system/context.js";
          s.async = true;
          t.parentNode.insertBefore(s, t);
        })(this, this.document, "yandexContextAsyncCallbacks");
      </script>
    </div>
    <div class="sectiontitle">
      <h6 class="heading">Новости компании RobotiX</h6>
      <p>Тут находятся новости нашей компании</p>
    </div>
    <div class="latest articlesLinks">        <article class="one_third article-link first" onclick="linkClick('update-2020.12.25')">
        <figure><a target="_blank"><img src="images/articles/update.25.12.2020.png" alt=""></a>
          <figcaption>
            <time><strong style="padding: 12px 0;"><i class="fa  fa-newspaper-o"></i></strong></time>
          </figcaption>
        </figure>
        <div class="txtwrap">
          <h4 class="heading">Большое обновление!</h4>
          <p>Новый контент, новые функции и многое другое!</p>
          <footer><a target="_blank">Больше »</a></footer>
        </div>
      </article>        <article class="one_third article-link" onclick="linkClick('pereezd')">
        <figure><a target="_blank"><img src="images/articles/pereezd.jpg" alt=""></a>
          <figcaption>
            <time><strong style="padding: 12px 0;"><i class="fa  fa-newspaper-o"></i></strong></time>
          </figcaption>
        </figure>
        <div class="txtwrap">
          <h4 class="heading">Переезд</h4>
          <p>Наш сайт переехал на новый хостинг! Теперь наш адресс <span style="width: max-content; display: inline-block;">robotix-com.web.app!</span></p>
          <footer><a target="_blank">Больше »</a></footer>
        </div>
      </article>        <article class="one_third article-link" onclick="linkClick('robotixLogo')">
        <figure><a target="_blank"><img src="images/articles/robotixLink.jpg" alt=""></a>
          <figcaption>
            <time><strong style="padding: 12px 0;"><i class="fa  fa-newspaper-o"></i></strong></time>
          </figcaption>
        </figure>
        <div class="txtwrap">
          <h4 class="heading">Новый логотип</h4>
          <p>Встречайте новый логотип нашей компании!</p>
          <footer><a target="_blank">Больше »</a></footer>
        </div>
      </article></div>
    <button class="btn" onclick="goTo('pages/company-news.html', true);">Больше &gt;&gt;</button>
    <div class="sectiontitle">
      <h6 class="heading">Наши статьи</h6>
      <p>Самые свежие статьи наших авторов</p>
    </div>
    <div class="latest articlesLinks">        <article class="one_third article-link first" onclick="linkClick('eco')">
        <figure><a target="_blank"><img src="images/articles/eco.png" alt=""></a>
          <figcaption>
            <time><strong style="padding: 12px 0;"><i class="fa fa-picture-o"></i></strong></time>
          </figcaption>
        </figure>
        <div class="txtwrap">
          <h4 class="heading">Берегите природу!</h4>
          <p>Новый арт от Михаила Сизова на тему экологии!</p>
          <footer><a target="_blank">Больше »</a></footer>
        </div>
      </article>        <article class="one_third article-link" onclick="linkClick('eva%60sRobot')">
        <figure><a target="_blank"><img src="images/articles/eva%60sRobotMini.jpg" alt=""></a>
          <figcaption>
            <time><strong style="padding: 12px 0;"><i class="fa fa-picture-o"></i></strong></time>
          </figcaption>
        </figure>
        <div class="txtwrap">
          <h4 class="heading">Робот от Эвелины</h4>
          <p>Новый арт от наших художников!</p>
          <footer><a target="_blank">Больше »</a></footer>
        </div>
      </article>        <article class="one_third article-link" onclick="linkClick('onlyUOur')">
        <figure><a target="_blank"><img src="images/articles/onlyUOur.jpg" alt=""></a>
          <figcaption>
            <time><strong style="padding: 12px 0;"><i class="fa  fa-video-camera"></i></strong></time>
          </figcaption>
        </figure>
        <div class="txtwrap">
          <h4 class="heading">Только у нас</h4>
          <p>Концерт Михаила Задорнова "Только у нас"</p>
          <footer><a target="_blank">Больше »</a></footer>
        </div>
      </article></div>
    <button class="btn" onclick="goTo('pages/articles.html', true);">Больше &gt;&gt;</button>
    <div class="sectiontitle">
      <h6 class="heading">Галерея</h6>
      <p>На этой странице вы можете увидеть фотографии наших проектов</p>
    </div>
    <ul class="nospace clear images-container">
    <li class="one_quarter {{CLASSFIRST}}" onclick="openImage('images/special/christmas.jpg', 'Новый год от Миши')">
        <div class="img" style="background-image: url('images/special/christmas.jpg')">
            <a>
                <img style="visibility: hidden" src="https://firebasestorage.googleapis.com/v0/b/robotix-com.appspot.com/o/images%2Fgallery%2F01.png?alt=media&amp;token=0af54878-e691-4056-a3ec-70e9c2aa428b">
            </a>
            <div class="img-title">Новый год от Миши</div>
            <div class="date">23.12.2020</div>
        </div>
        <div class="header">Новый год от Миши</div>
    </li>

    <li class="one_quarter {{CLASSFIRST}}" onclick="openImage('images/articles/eco1.png', 'Экология от Миши')">
        <div class="img" style="background-image: url('images/articles/eco1.png')">
            <a>
                <img style="visibility: hidden" src="https://firebasestorage.googleapis.com/v0/b/robotix-com.appspot.com/o/images%2Fgallery%2F01.png?alt=media&amp;token=0af54878-e691-4056-a3ec-70e9c2aa428b">
            </a>
            <div class="img-title">Экология от Миши</div>
            <div class="date">23.12.2020</div>
        </div>
        <div class="header">Экология от Миши</div>
    </li>

    <li class="one_quarter {{CLASSFIRST}}" onclick="openImage('images/gallery/red-october-avrora.jpg', 'День Великой Октябрьской Социалистической Революции')">
        <div class="img" style="background-image: url('images/gallery/red-october-avrora.jpg')">
            <a>
                <img style="visibility: hidden" src="https://firebasestorage.googleapis.com/v0/b/robotix-com.appspot.com/o/images%2Fgallery%2F01.png?alt=media&amp;token=0af54878-e691-4056-a3ec-70e9c2aa428b">
            </a>
            <div class="img-title">День Великой Октябрьской Социалистической Революции</div>
            <div class="date">04.11.2020</div>
        </div>
        <div class="header">День Великой Октябрьской Социалистической Революции</div>
    </li>

    <li class="one_quarter {{CLASSFIRST}}" onclick="openImage('images/gallery/den-narodnogo-edinstva.gif', 'День народного единства')">
        <div class="img" style="background-image: url('images/gallery/den-narodnogo-edinstva.gif')">
            <a>
                <img style="visibility: hidden" src="https://firebasestorage.googleapis.com/v0/b/robotix-com.appspot.com/o/images%2Fgallery%2F01.png?alt=media&amp;token=0af54878-e691-4056-a3ec-70e9c2aa428b">
            </a>
            <div class="img-title">День народного единства</div>
            <div class="date">04.11.2020</div>
        </div>
        <div class="header">День народного единства</div>
    </li>
</ul>
    <button class="btn" onclick="goTo('pages/gallery.html', true);">Больше &gt;&gt;</button>
    <div class="sectiontitle">
      <h6 class="heading">Страницы о программировании</h6>
      <p>Здесь находятся наши страницы о программированием</p>
    </div>
    <div class="margin--30"></div>
    <div class="recommendations">
      <div class="one_third first" title="Программирование сегодня">
        <a href="pages/programming/historyProgramming.html">
          <img src="images/errors/historyProgramming.png">
          <span class="recommendation-button">История программирования</span>
        </a>
      </div>
      <div class="one_third" title="Успешные программисты">
        <a href="pages/programming/historyProgrammer.html">
          <img src="images/errors/historyProgrammer.png">
          <span class="recommendation-button">Успешные программисты</span>
        </a>
      </div>
      <div class="one_third" title="Полезные программы">
        <a href="pages/programming/good-programs.html">
          <img src="images/errors/good-programs.png">
          <span class="recommendation-button">Полезные программы</span>
        </a>
      </div>
    </div>
    <div class="margin-30"></div>
    <div class="sectiontitle">
      <h6 class="heading">Наши услуги</h6>
      <p>Здесь расположены услуги нашей компании</p>
    </div>
    <div class="margin--30"></div>
    <div class="recommendations">
      <div class="one_third first" title="Сайты на заказ">
        <a href="pages/sites.html">
          <img src="images/errors/sites.png">
          <span class="recommendation-button">Сайты на заказ</span>
        </a>
      </div>
      <div class="one_third" title="Чат-боты на заказ">
        <a href="pages/u-Chat-bot.html">
          <img src="images/errors/02.png">
          <span class="recommendation-button">Чат-боты на заказ</span>
        </a>
      </div>
      <div class="one_third" title="Другие услуги">
        <a href="pages/uslugi.html">
          <img src="images/errors/uslugi.png">
          <span class="recommendation-button">Другие услуги</span>
        </a>
      </div>
    </div>
    <div class="margin-30"></div>
    <div class="sectiontitle">
      <h6 class="heading">Новости робототехники</h6>
      <p>Здесь вы найдёте новости робототехники</p>
    </div>
    <div class="group latest">        <article class="one_third first">
        <figure><a href="https://robo-hunter.com/news/robot-rocycle-sortiruet-plastik-i-bumagu-dlya-pererabotki16862" target="_blank"><img src="images/news/robo/sort.jpeg" alt=""></a>
          <figcaption>
            <time><strong>18</strong><em>Августа</em></time>
          </figcaption>
        </figure>
        <div class="txtwrap">
          <h4 class="heading">Сортировщик мусора</h4>
          <p>Робот RoCycle сортирует пластик и бумагу для переработки[…]</p>
          <footer><a href="https://robo-hunter.com/news/robot-rocycle-sortiruet-plastik-i-bumagu-dlya-pererabotki16862" target="_blank">Больше »</a></footer>
        </div>
      </article>        <article class="one_third">
        <figure><a href="https://robo-hunter.com/news/robot-nauchilsya-narezat-ovoshi-na-lomtiki-opredelennoi-tolshini16745" target="_blank"><img src="images/news/robo/cooker.png" alt=""></a>
          <figcaption>
            <time><strong>18</strong><em>Августа</em></time>
          </figcaption>
        </figure>
        <div class="txtwrap">
          <h4 class="heading">Робот-повар</h4>
          <p>Робот научился нарезать овощи на ломтики определенной толщины[…]</p>
          <footer><a href="https://robo-hunter.com/news/robot-nauchilsya-narezat-ovoshi-na-lomtiki-opredelennoi-tolshini16745" target="_blank">Больше »</a></footer>
        </div>
      </article>        <article class="one_third">
        <figure><a href="https://ria.ru/20200727/1574970854.html" target="_blank"><img src="images/news/robo/moon.jpg" alt=""></a>
          <figcaption>
            <time><strong>18</strong><em>Августа</em></time>
          </figcaption>
        </figure>
        <div class="txtwrap">
          <h4 class="heading">Роботы на Луне</h4>
          <p>Российскую лунную базу построят роботы, заявил разработчик "Федора"[…]</p>
          <footer><a href="https://ria.ru/20200727/1574970854.html" target="_blank">Больше »</a></footer>
        </div>
      </article></div>
    <button class="btn" onclick="goTo('pages/robototehnic/news.html', true);">Больше &gt;&gt;</button>
    <div class="clear"></div>
  </section>
  <div class="ads-container">
  <!-- Yandex.RTB R-A-659817-1 -->
<div id="yandex_rtb_R-A-659817-1"></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-659817-1",
                renderTo: "yandex_rtb_R-A-659817-1",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script>
  </div>
</div>
<!--Footer-->
<div id="footerContainer">
<div class="wrapper row4">
  <footer id="footer" class="hoc clear">
    <div class="one_quarter first">
      <h6 class="heading">Новости</h6>
      <ul class="nospace linklist">

        <li>
          <article>
            <h2 class="nospace font-x1"><a>Большое обновление!</a></h2>
            <time class="font-xs block btmspace-10" datetime="2020-12-25">25 декабря 2020</time>
            <p class="nospace">Новый контент, новые функции и многое другое!</p>
          </article>
        </li>

        <li>
          <article>
            <h2 class="nospace font-x1"><a>Переезд</a></h2>
            <time class="font-xs block btmspace-10" datetime="2020-10-25">25 Октября 2020</time>
            <p class="nospace">Наш сайт переехал на новый хостинг! Теперь наш адресс <span style="width: max-content; display: inline-block;">robotix-com.web.app!</span></p>
          </article>
        </li>

      </ul>
    </div>
    <div class="one_quarter">
     <h6 class="heading">Полезные источники</h6>
     <ul class="nospace linklist">
        <li>
          <article>
            <h2 class="nospace font-x1"><a>Мы советуем посетить вам сайты:</a></h2><br>
            <p class="nospace">Мы советуем посетить вам сайты:</p>
            <a class="nospace" target="_blank" href="http://ardronos.ru/?i=1">ardronos.ru</a><br>
            <a class="nospace" target="_blank" href="https://a-kurlaev.bitbucket.io/right-eating/">a-kurlaev.bitbucket.io/right-eating/</a><br>
            <a class="nospace" target="_blank" href="http://programms.hostronavt.ru">programms.hostronavt.ru</a>
          </article>
        </li>
      </ul>
    </div>
    <div class="one_quarter">
     <h6 class="heading">Наши партнёры</h6>
     <ul class="nospace linklist">
        <li>
          <article>
            <a target="_blank" href="#">
                <img src="./images/partner/kurlaCompany.png" style="width: 150px;">
            </a>
          </article>
        </li>
      </ul>
      <div class="footer__plateg-container">
        <div class="footer__plateg"><div style="padding-bottom: 7px">Поддержите нас!</div><a class="footer__donate-button" href="./pages/donate.html">Пожертвовать</a></div>
        <div class="footer__plateg">Мы поддерживаем:<br>
          <a href="https://payeer.com" class="plateg-link"><img src="./images/plateg/payeer.png"></a>
          <a href="https://money.yandex.ru/" class="plateg-link"><img src="./images/plateg/yandex.png"></a>
        </div>
      </div>
    </div>
    <div class="one_quarter">
      <h6 class="heading">Контакты</h6>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
          Новосибирск, Космическая 10
          </address>
        </li>
		<li><i class="fa fa-envelope-o"></i><a href="mailto:robotiXcom@yandex.ru" style="color: #A8A7A6;">robotiXcom@yandex.ru</a></li>
        <li><i class="fa fa-envelope-o"></i><a href="mailto:kurlaev.alex.linux@gmail.com" style="color: #A8A7A6;">kurlaev.alex.linux@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li><a target="_blank" class="faicon-facebook" href="https://www.facebook.com/RobotiXKurla-company-100911808289457/?view_public_for=100911808289457"><i class="fa fa-facebook"></i></a></li>
        <li><a target="_blank" class="faicon-twitter" href="https://twitter.com/RobotixC"><i class="fa fa-twitter"></i></a></li>
        <li><a target="_blank" class="faicon-vk" href="https://vk.com/public195027115"><i class="fa fa-vk"></i></a></li>
        <li><a target="_blank" class="faicon-instagram" href="https://www.instagram.com/robotixkurla/"><i class="fa fa-instagram"></i></a></li>
		<li><a target="_blank" class="faicon-youtube" href="https://youtube.com"><i class="fa fa-youtube-play"></i></a></li>
      </ul>
    </div>
  </footer>
</div>
<div class="wrapper row5">
  <div id="copyright" class="hoc clear">
    <p class="fl_left">Новосибирск, 2020</p>
  </div>
</div>
<a id="backtotop" href="#top" class="visible"><i class="fa fa-chevron-up"></i></a>
</div>
<div id="big-image-container-wrapper">
  <div class="big-image-container" style="display: none">
    <div class="close">
      <div class="close-item" style="border-left: none;border-top: none;"></div>
      <div class="close-item" style="border-right: none;border-top: none;"></div>
      <div class="close-item" style="border-left: none;border-bottom: none;"></div>
      <div class="close-item" style="border-right: none;border-bottom: none;"></div>
    </div>
    <img class="big-image" src="../images/school/plitveckie-lakes.jpg">
  </div>
</div>
<div class="preloader-container" style="display: none;">
  <div class="preloader">
    <div class="block"><div class="square"></div></div>
    <div class="block"><div class="square"></div></div>
    <div class="block"><div class="square"></div></div>
    <div class="block"><div class="square"></div></div>
    <div class="block"><div class="square"></div></div>
    <div class="block"><div class="square"></div></div>
    <div class="block"><div class="square"></div></div>
    <div class="block"><div class="square"></div></div>
  </div>
</div>
<!-- JAVASCRIPTS -->
<script src="js/specialStyle.js"></script>
<script src="js/js-modules/sha512.min.js"></script>
<script src="js/js-modules/cookieFunctions.js"></script>
<script src="js/js-modules/functions.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.backtotop.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.22.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.22.0/firebase-firestore.js"></script>
<script src="js/adblocker.js"></script>

<script src="templates/header.js"></script>
<script src="templates/headerTemplate.js"></script>
<script src="templates/footer.js"></script>
<script src="js/header.js"></script>
<script src="js/inputAppFooter.js"></script>
<script src="js/deletePreloader.js"></script>

<script src="templates/themes.js"></script>
<script src="templates/roboNews.js"></script>
<script src="templates/newsTemplate.js"></script>
<script src="templates/articleTemplate.js"></script>
<script src="templates/companyNews.js"></script>
<script src="templates/galleryTemplate.js"></script>
<script src="templates/gallery.js"></script>

<script src="js/themes.js"></script>
<script src="js/inputApp.js"></script>
<script src="js/inputAppArtical.js"></script>
<script src="js/inputAppGallery.js"></script>
<!-- The core Firebase JS SDK is always required and must be listed first -->

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="js/ips.js"></script>


<div>
<div class="settings-button-container" style="display: flex;">
  <div class="settings-button" onclick="openSettingsMenu()">
    <i class="fa fa-chevron-left settings-button__icon"></i>
  </div>
</div>
<div id="settings-menu-wrapper" style="display: none;">

  <div class="settings-button-container__close" onclick="closeSettingsMenu()"><div class="settings-button-container" style="position: absolute;">
    <div class="settings-button">
      <i class="fa fa-chevron-right settings-button__icon"></i>
    </div>
  </div></div>
  <div class="settings-body">
    <div class="settings-menu">
      <div class="settings-menu__item">Тема</div>
      <div class="settings-menu__close-container">
        <i class="fa fa-close settings-menu__close-button" onclick="closeSettingsMenu()"></i>
      </div>
    </div>
      <div class="themes__checkbox">
          <input type="checkbox" checked="checked" onchange="localStorage.changeThemeOnHolidays = this.checked ? 1 : 0; inputAppStyle();" id="changeThemeOnHolidays">
          <label for="changeThemeOnHolidays">менять тему в течении праздников</label>
      </div>
    <div class="settings__themes">
    <div class="theme">
      <div style="background-image: url('images/special/standard.png');" class="theme__img"></div>
      <div class="theme__text">
        <h3 class="theme__text_header">Стандартная тема</h3>
        <div class="theme__text_description">Белый фон, обычный тёмно-серый шрифт, стандартная цветовая гамма
          <button class="btn theme__text_button" onclick="selectTheme('')">Выбрать</button>
        </div>
      </div>
    </div>

    <div class="theme">
      <div style="background-image: url('images/special/christmas.jpg');" class="theme__img"></div>
      <div class="theme__text">
        <h3 class="theme__text_header">Новогодняя тема</h3>
        <div class="theme__text_description">Новогодний фон, рождественский шрифт для заголовков и завитый шрифт для основного текста!
          <button class="btn theme__text_button" onclick="selectTheme('christmas')">Выбрать</button>
        </div>
      </div>
    </div>

    <div class="theme">
      <div style="background-image: url('images/special/halloween-backgroung.jpg');" class="theme__img"></div>
      <div class="theme__text">
        <h3 class="theme__text_header">Жуткая тема!</h3>
        <div class="theme__text_description">Мрачный фон, огненный шрифт для заголовков и паутинный шрифт для основного текста! В этой теме содержится дух Хеллоуина!
          <button class="btn theme__text_button" onclick="selectTheme('halloween')">Выбрать</button>
        </div>
      </div>
    </div>

    <div class="theme">
      <div style="background-image: url('images/special/united-Russia-backgroung-cropped.jpg');" class="theme__img"></div>
      <div class="theme__text">
        <h3 class="theme__text_header">День народного единства</h3>
        <div class="theme__text_description">Особый фон и оформление меню в стиле флага России
          <button class="btn theme__text_button" onclick="selectTheme('united-Russia')">Выбрать</button>
        </div>
      </div>
    </div>

    <div class="theme">
      <div style="background-image: url('images/special/red-october-background.jpg');" class="theme__img"></div>
      <div class="theme__text">
        <h3 class="theme__text_header">День Октябрьской революции!</h3>
        <div class="theme__text_description">Фон в революционном стиле, советский шрифт и красный вместо оранжевого цвета!
          <button class="btn theme__text_button" onclick="selectTheme('red-october')">Выбрать</button>
        </div>
      </div>
    </div>
</div>
  </div>
</div></div><script src="templates/articles.js" onload="inputAppArticle()"></script></body>